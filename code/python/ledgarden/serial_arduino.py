#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  Define the serial interface.

"""

# Setup the basic logging configuration.
from mirutil import logging

import time
import serial
import glob

import atexit
import threading

from . import common

log = logging.getLogger(__name__)

ser = None
byteio = None


def closeArduino():
    global ser, textio, byteio
    print('Closing arduino connection.')

    if (ser is not None) and (ser.is_open):
        ser.close()

def connectToArduino():
    global ser, textio, byteio

    # Check te see if our connection is already active.
    if (ser is not None) and (ser.is_open):
        return

    # See if we can find an arduino.
    # This is totally specific for my OS X setup.
    arduino_port = None
    usbport_search = [glob.glob('/dev/tty.usbmodem*')
                      ,glob.glob('/dev/ttyACM*')]
    for port in usbport_search:
        if port:
          arduino_port = port[0]
            
    if arduino_port is None:
        raise Exception('Could not find a usb port with a conneced arduino.')

    log.info('Initializing Serial connection to Arduino.')
    log.info('Thread: {}'.format(threading.current_thread()))

    # Setup our serial connection.
    #ser = serial.Serial(arduino_port, 115200, timeout=0.1)
    ser = serial.Serial(arduino_port, 1000000, timeout=None, write_timeout=None)
    time.sleep(0.5)

    byteio = ser


atexit.register(closeArduino)