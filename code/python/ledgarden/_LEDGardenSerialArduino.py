

# Setup the basic logging configuration.
from mirutil import logging

import numpy as np

import struct
import time

from . import common
from . import serial_arduino

class LEDGardenSerialArduino:
    """
    Communicate back and forth with the arduino board.

    Since I am likely bandwith limited I am using some acronyms for the commands.

    Warning:  By default the arudino is setup with a 64 bit serial buffer.
              This was too small to handle the entire LED array.  I have modified
              HardwareSerial.h inside Arduino_v1.5.7.app, as well as boards.txt
              to increase the buffer size to 256 bits.  This is good enough for
              the Heartpath Object as currently written, but care must be taken
              not to inadvertantly overflow the buffer.
              Reference: http://www.hobbytronics.co.uk/arduino-serial-buffer-size

    Note about speed.  It looks like I can increase the comunication speed, but
    only for short data bursts.
    """

    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)
        self.line_buffer = ''


    # This does not work when the serial connection is in a module.
    #def __del__(self):
    #    print('Closing the serial conneciton.')
    #    serial_arduino.ser.close()


    def connect(self):
        # # See if we can find an arduino.
        # # This is totally specific for my OS X setup.
        # usbports = glob.glob('/dev/cu.usbmodem*')
        # if not usbports:
        #     raise Exception('Could not find a usb port with a conneced arduino.')
        #
        # arduino_port = usbports[0]
        #
        # # Setup our serial connection.
        # #ser = serial.Serial(arduino_port, 115200, timeout=0.1)
        # #self.ser = serial.Serial(arduino_port, 250000, timeout=0.005)
        # self.ser = serial.Serial(arduino_port, 1000000, timeout=0.005)
        #
        # # Important Note:  The buffer for the byteio is separate from the byteio.
        # #                  So while it is ok to write to either one interchageably
        # #                  I can only read from the byteio.  Obviously I could
        # #                  turn buffering off if I really need to.
        # self.byteio = io.BufferedRWPair(self.ser, self.ser)
        # self.byteio = io.byteioWrapper(self.byteio, newline='\n', errors='ignore', encoding='ascii')
        serial_arduino.connectToArduino()


    def clearAllBuffers(self):
        """Completely clear all of the read and write buffers."""
        serial_arduino.ser.flushInput()
        serial_arduino.ser.flushOutput()

        serial_arduino.byteio.flush()
        dummy = serial_arduino.byteio.read()


    def getNextResponse(self):
        try:
            #self.log.debug('Reading response from serial conneciton.')
            #self.log.debug('Thread: {}'.format(int(QtCore.QThread.currentThreadId())))
            #self.line = self.readNextLine()
            num_avail = serial_arduino.ser.out_waiting
            if num_avail == 0:
                num_avail = 1
            #print('Num Available: {}'.format(num_avail))
            self.line = serial_arduino.ser.read(num_avail).decode()
            #print(self.line)
        except UnicodeDecodeError:
            self.log.warning('Could not decode arduino response.')
            return None

        if self.line:
            #self.log.debug('Response recieved:')
            #self.log.debug(self.line)
            self.line_buffer += self.line
            # Make sure we actually recieved a newline.
            if '\n' in self.line_buffer:
                # This should always result in in an empty string in self.line_buffer
                command, self.line_buffer = self.line_buffer.split('\n', maxsplit=1)
                return self.parseCommands(command)

        return None


    def sendCommand(self, command, value=None):
        """
        Send a command to the Arduino.

        The command should be a string.  Optionally a value can be passed as well.
        """
        packet = command.encode() + b'\n'
        if isinstance(value, bool):
            packet += struct.pack('!B', value)
        else:
            raise Exception('Data type not implemented.')
        self.writeBytes(packet)


    def parseCommands(self, command):
        #print('PYTHON RECIEVE: {}'.format(command))

        if command == 'OK':
            return ('OK', True)
        if command == 'NEXT':
            return ('NEXT', True)
        if command == 'BUTTON':
            return ('BUTTON', self.readButtonState())
        else:
            return ('UNKNOWN', True)


    def readNextLine(self):
        #serial_arduino.byteio.flush()
        line = serial_arduino.byteio.readline()
        if line != '':
            return(line)


    def writeBytes(self, bytestring):
        serial_arduino.byteio.write(bytestring)
        serial_arduino.byteio.flush()
        #print('PYTHON SEND: '+text.decode('ascii'))


    def writeText(self, text):
        #self.log.debug('Writing text to serial conneciton: {}'.format(text))
        #self.log.debug('Thread: {}'.format(int(QtCore.QThread.currentThreadId())))

        num_bytes = serial_arduino.byteio.write(text.encode())
        serial_arduino.byteio.flush()
        #print('PYTHON SEND: '+text)


    def writeInt(self, integer):
        a = struct.pack('!H', integer)
        serial_arduino.byteio.write(a)
        serial_arduino.byteio.flush()


    def writeIntArray(self, intarray):
        a = struct.pack('!{}H'.format(len(intarray)), *intarray)
        #print('Packet size: {}'.format(len(a)))
        serial_arduino.byteio.write(a)
        serial_arduino.byteio.flush()


    def writeByteArray(self, intarray):
        a = struct.pack('!{}B'.format(len(intarray)), *intarray)
        #print('Packet size: {}'.format(len(a)))
        serial_arduino.byteio.write(a)
        serial_arduino.byteio.flush()


    def writeStringArray(self, array):
        a = struct.pack('!{}s'.format(len(array)), array)
        #print('Packet size: {}'.format(len(a)))
        serial_arduino.byteio.write(a)
        serial_arduino.byteio.flush()


    def writeLEDValues(self, led_state):
        """
        Send the LED values to the Arduino over the serial connection.

        Before we send the law LED values we convert them from floating
        point to an integer value using the value in common.kLEDNumLevels.

        Programming notes:
          With the Raspberry Pi writing is slow enough that I don't appear
          to need to limit the packet size, or wait for responses.  By
          sending a single packet I can reduce the transfer time a bit.

          On my MacBook Pro, both of these become necessary.
        """
        time_start = time.time()
        time_sum_wait = 0.0
        time_sum_send_info = 0.0
        time_sum_pack_array = 0.0
        time_sum_write_array = 0.0
        time_sum_loop = 0.0

        num_leds = led_state['color'].shape[0]

        # The packet size is the number of LED states that we will send in a single burst.
        packet_size_request = 50
        packet_size = packet_size_request - (num_leds % packet_size_request)

        time_000 = time.time()
        outputintarray = np.empty(packet_size*common.kLEDNumColors, dtype=int)
        time_001 = time.time()

        for ii_start in range(0, num_leds, packet_size):
            time_00 = time.time()
            self.waitForArduinoLoopNext()
            time_01 = time.time()

            # I can save a few ms by packing this all together and sending it once.
            #self.writeText('LED\n')
            #self.writeInt(ii_start)
            #self.writeInt(packet_size)
            text = b'LED\n'+struct.pack('!H', ii_start)+struct.pack('!H', packet_size)
            self.writeBytes(text)
            time_02 = time.time()

            # Pack all the LEDs using three bytes (24 bit color).
            outputintarray[:] = led_state['color'].flat[ii_start*common.kLEDNumColors:(ii_start+packet_size)*common.kLEDNumColors]*common.kLEDNumLevels

            time_03 = time.time()

            self.writeByteArray(outputintarray)

            time_04 = time.time()

            time_sum_wait += time_01 - time_00
            time_sum_send_info += time_02 - time_01
            time_sum_pack_array += time_03 - time_02
            time_sum_write_array += time_04-time_03
            time_sum_loop += time_04 - time_00

        #self.waitForArduinoLoopNext()
        self.writeText('DONE\n')

        time_init_array = time_001 - time_000
        time_writeLEDValues = time.time() - time_start

        if False:
            self.log.debug('Python writeLEDValues:  {} ms, time_init_array:  {} ms, time_sum_wait:  {} ms, time_send_info:  {} ms, time_pack_array: {} ms, time_sum_write_array: {} ms, time_sum_loop: {} ms'.format(
                int(time_writeLEDValues*1000.)
                ,int(time_init_array*1000.)
                ,int(time_sum_wait*1000.)
                ,int(time_sum_send_info*1000.)
                ,int(time_sum_pack_array*1000.)
                ,int(time_sum_write_array*1000.)
                ,int(time_sum_loop*1000.)))


    def getExpectedResponse(self, num_bytes):
        """
        Wait for an expected response with a set number of bytes.
        """
        while True:
            response = serial_arduino.byteio.read(num_bytes)

            if response:
                return response


    def readButtonState(self):
        num_buttons = struct.unpack('B', self.getExpectedResponse(1))[0]
        button_state = []
        #print('Num buttons: {}'.format(num_buttons))
        for ii in range(num_buttons):
            state = struct.unpack('?', self.getExpectedResponse(1))[0]
            button_state.append(state)

        #print('Button state: ', button_state)

        return button_state


    def requestReady(self):
        self.writeText('REQUESTREADY\n')


    def waitForArduinoLoopNext(self):
        """
        Wait for the arduino to send "NEXT", meaning it is ready for
        another packet of LED values.
        """
        timeout = 0.5
        time_last_request = time.time()

        #serial_arduino.byteio.flush()
        while True:
            response = self.getNextResponse()
            if response:
                if response[0] == 'NEXT':
                    return
            else:
                if (time.time() - time_last_request) > timeout:
                    raise Exception('Timeout.')

                #time.sleep(common.k_serial_refresh_time)


    def waitForArduinoButtonUpdate(self):
        """
        Wait for the arduino to send "BUTTON", then read the button values.
        """
        timeout = 0.5
        time_last_request = time.time()

        #serial_arduino.byteio.flush()
        while True:
            response = self.getNextResponse()
            if response:
                if response[0] == 'BUTTON':
                    return response[1]
            else:
                if (time.time() - time_last_request) > timeout:
                    raise Exception('Timeout.')

                #time.sleep(common.k_serial_refresh_time)

