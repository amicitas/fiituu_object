#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  The global configuration for the LED garden project.

"""


import time
from collections import OrderedDict
import numpy as np

# Setup the user options
options = OrderedDict()
# A global speed multiplier.  This is implemented independently by each pattern,
# so it may or not have an effect.
options['speed'] = 0
options['brightness'] = 1.0
options['num_buttons'] = 5
options['arduino_usb_connection'] = True
options['enable_simulator'] = False
# Color correction values should never be greater than 1.0.
options['color_correction'] = [1.0, 0.6, 0.75]
options['color_shift'] = 0.0
options['color_swap'] =  0.0
options['debug_cycle'] = False

# The mirroring mode.
options['mirror_mode_reversed'] = True


# Declare some module level variables.
mCycleCurrent = 0
mTimeStart = 0.0
mRuntimeCycleCurrent = 0.0
mRuntimeCycleLast = 0.0

# Declare module level constant variables.
kNumLEDs = 150

kRed = 'R'
kWhite = 'W'
kUv = 'U'
kLEDNumColors = 3

kLEDMinValue = 0.0
kLEDMaxValue = 1.0

# This will control how the floating point LED values are
# are converted to integer values before being sent over
# the serial connection.
kLEDNumLevels = 255

# Declare some default program option.
kProgramLifetime = 15.0

# Limit the number of programs that can be run at one time.
# The appropriate value is dependent on system performance.
k_max_running_programs = 20

# This is used by individual programs to limit creation of patterns.
# It is handled by each program individually, so it is not guaranteed
# to implemented correctly for the application as a whole.
# The total max number of patterns will theoretically be:
# k_max_running_programs * k_max_program_patterns.
k_max_program_patterns = 30

# It may be possible to get a slight speedup on some systems by using 'Float32'.
# Care must be taken that any system time values are always represented as 'Float64'.
k_default_dtype = np.dtype('Float32')

# Declare module level functions.

def runtimeCycle():
    """Return the current cycle time."""
    global mRuntimeCycleCurrent
    return mRuntimeCycleCurrent


def runtime():
    """Return the current runtime."""
    global mTimeStart
    return time.time() - mTimeStart


def getSpeedFactor():
    """Return a standard speed factor from the integer speed value."""
    return 1.10**options['speed']