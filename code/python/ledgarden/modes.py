#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  Define a set of modes for the LED garden.

"""

from mirutil import logging

from collections import OrderedDict
from .programs import *
from . import color
from . import common

class LEDGardenModesCollection():

    def __init__(self, garden=None):
        self.log = logging.getLogger(self.__class__.__name__)

        self.garden = garden

        self.modes = OrderedDict()
        self.modes['Bubbler'] = ModeBubbler(self.garden)
        self.modes['Spot Sender'] = ModeSpotSender(self.garden)
        self.modes['Rainbow Burst'] = ModeRainbowBurst(self.garden)
        self.modes['Strobe'] = ModeStrobeLight(self.garden)
        self.modes['Hotaru Hanami'] = ModeHotaruHanami(self.garden)
        self.modes['Fire'] = ModeFire(self.garden)
        #self.modes['Test'] = ModeTest(self.garden)

        self.selectMode()


    def selectMode(self, name=None):

        if name is None:
            #self.current_mode = 'Spot Sender'
            #self.current_mode = 'Bubbler'
            #self.current_mode = 'Rainbow Burst'
            #self.current_mode = 'Hotaru Hanami'
            #self.current_mode = 'Strobe'
            self.current_mode = 'Fire'
            #self.current_mode = 'Test'
        else:
            self.current_mode = name

        self.log.info('Mode selected: {}'.format(self.current_mode))


    def getNextMode(self, reverse=False):
        """
        Cycle to the next defined mode.
        If we reach the last mode, then start again from the beginning.
        """
        modes = list(self.modes.keys())
        index = modes.index(self.current_mode)

        if reverse:
            index -= 1
            if index < 0:
                index = len(modes)-1
        else:
            index += 1
            if index >= len(modes):
                index = 0

        new_mode = modes[index]

        return new_mode


    def activate(self):
        """
        Activate the current mode.
        This should only be called after the previous mode was deactivated.
        """
        self.modes[self.current_mode].activate()


    def getBackgroundPrograms(self):
        return self.modes[self.current_mode].background_programs


    def getIdlePrograms(self):
        return self.modes[self.current_mode].idle_programs


    def getButtonPrograms(self):
        return self.modes[self.current_mode].button_programs


    def run(self):
        self.buttonStatusHandler()


    def buttonStatusHandler(self):
        self.modes[self.current_mode].buttonStatusHandler()


    def actionButtonPress(self, ii):
        self.modes[self.current_mode].actionButtonPress(ii)


    def actionCrazyMode(self):
        self.modes[self.current_mode].actionCrazyMode()


class LEDGardenMode():

    def __init__(self, garden=None):

        self.log = logging.getLogger(self.__class__.__name__)

        # Save a reference to the main garden object.
        self.garden = garden

        # Define background programs.
        self.background_programs = []

        # Define idle programs.
        self.idle_programs = []

        # Define button programs.
        self.button_programs = OrderedDict()
        for ii in range(5):
            self.button_programs[ii] = []

        # Define a standard holder for other programs.
        self.other_programs = []

        # Define a default crazy program.
        self.crazy_programs = []
        #self.crazy_programs.append((ProgramCrazyProgram, None))
        self.crazy_programs.append((ProgramEmikoTchstTchst, None))


    def activate(self):
        pass


    def checkForButtonPush(self):
        """
        Check to see if there are any new button presses.
        Returns a status array.
        """
        new_press = [self.garden.button_state[ii]['state_current'] and (not self.garden.button_state[ii]['state_last'])
                     for ii in range(len(self.garden.button_state))]

        new_press = np.array(new_press)

        # for ii in range(len(self.garden.button_state)):
        #     self.log.debug('Button: {}'.format(ii))
        #     self.log.debug('Last: {}, Current: {}, New: {}'.format(self.garden.button_state[ii]['state_last']
        #     , self.garden.button_state[ii]['state_current']
        #     , self.log.debug(new_press[ii])))

        return new_press


    def buttonCommandsHandler(self
                              ,press_status
                              ,enable_brightess=True
                              ,enable_speed=True
                              ,enable_color=True
                              ):
        """
        A standard handler for multi-button commands.
        This can reimplemented by individual modes to map
        multi-button presses to different actions.

        Standard commands:
          0+1H:    Speed up.
          2+1H:    Slow down.
          1+0H:    Increment color shift.
          1+2H:    Increment color swap.
          0+2H:    Increase Brightness.
          2+0H:    Decrease Brightness.
          0+1H+2H: Move to next mode.
          2+0H+1H: Toggle the mirror mode.
          1+0H+2H: Reset.

        """
        #self.log.debug('Checking for button commands.')

        handled = False
        if press_status[0] and press_status[1] and press_status[2]:
            self.log.debug('Button 0+1+2 pressed. Go crazy!')
            self.actionCrazyMode()
            handled = True
        elif press_status[0] and self.garden.button_state[1]['state_current'] and self.garden.button_state[2]['state_current']:
            self.log.debug('Button 0+1H+2H pressed.')
            self.garden.cycleToNextMode()
            handled = True
        elif press_status[2] and self.garden.button_state[0]['state_current']and self.garden.button_state[1]['state_current']:
            self.log.debug('Button 2+0H+1H pressed.')
            self.garden.toggleMirrorMode()
            handled = True
        elif press_status[1] and self.garden.button_state[0]['state_current']and self.garden.button_state[2]['state_current']:
            self.log.debug('Button 1+0H+2H pressed.')
            self.garden.resetOptions()
            handled = True
        elif press_status[0] and self.garden.button_state[1]['state_current']:
            self.log.debug('Button 0+1H pressed.')
            if enable_speed:
                self.garden.increaseSpeed()
                handled = True
        elif press_status[2] and self.garden.button_state[1]['state_current']:
            self.log.debug('Button 2+1H pressed.')
            if enable_speed:
                self.garden.decreaseSpeed()
                handled = True
        elif press_status[1] and self.garden.button_state[0]['state_current']:
            self.log.debug('Button 1+0H pressed.')
            if enable_color:
                self.garden.increaseColorShift()
                handled = True
        elif press_status[1] and self.garden.button_state[2]['state_current']:
            self.log.debug('Button 1+2H pressed.')
            if enable_color:
                self.garden.increaseColorSwap()
                handled = True
        elif press_status[0] and self.garden.button_state[2]['state_current']:
            self.log.debug('Button 0+2H pressed.')
            if enable_brightess:
                self.garden.increaseBrightness()
                handled = True
        elif press_status[2] and self.garden.button_state[0]['state_current']:
            self.log.debug('Button 2+0H pressed.')
            if enable_brightess:
                self.garden.decreaseBrightness()
                handled = True


        return handled


    def buttonStatusHandler(self):
        """
        This is the standard handler for button presses.
        This will handle standard multi button command sequences,
        and launch pre-defined button programs.

        For more control of button behavior this can reimplemented
        for individual modes.
        """
        #self.log.debug('Starting the button handler.')

        press_status = self.checkForButtonPush()
        handled = self.buttonCommandsHandler(press_status)

        if not handled:
            for ii, button in enumerate(self.garden.button_state):
                if press_status[ii]:
                    self.log.debug('Button {} was pressed.'.format(ii))
                    self.actionButtonPress(ii)


    def actionButtonPress(self, ii):
        """
        Take action when a button press is detected.

        The default action is to start the programs defined in button_programs.
        This method can be reimplemented to take different actions.
        """

        for prog in self.button_programs[ii]:
            self.garden.startActionProgram(prog)


    def actionCrazyMode(self):
        for prog in self.crazy_programs:
            self.garden.startActionProgram(prog)


class ModeHotaruHanami(LEDGardenMode):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Define background programs.
        options = {}
        options['fraction_active'] = 0.2
        self.idle_programs.append((ProgramHotaru, None))


        options = {}
        options['use_gradient'] = True
        options['random_gradient'] = True
        self.button_programs[0] = [
            (ProgramHanami, options)
            ]

        options = {}
        options['sparkler'] = True
        options['random_gradient'] = True
        self.button_programs[1] = [
            (ProgramHanami, options)
            ]

        options = {}
        options['random_gradient'] = True
        self.button_programs[2] = [
            (ProgramHanami, options)
            ]


class ModeStrobeLight(LEDGardenMode):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        options = {}
        options['cycles_active'] = 1
        options['cycles_inactive'] = 0
        options['end_after_lifetime'] = False
        self.idle_programs.append((ProgramStrobeLight, options))

        self.num_colors = 10
        self.color_values = np.linspace(0.0, 1.0, self.num_colors)
        self.color_index = 0

        self.garden.resetSpeed()



    def actionButtonPress(self, ii):


        if ii == 0:
            self.garden.increaseSpeed()

            # For this mode speed can only be negative.
            if common.options['speed'] > 0:
                self.garden.resetSpeed()


        if ii == 1:
            self.garden.deactivateIdlePrograms(fadeout_time=1.0)

            self.color_index += 1
            if self.color_index >= self.num_colors*2:
                self.color_index = 0

            if self.color_index >= self.num_colors:
                local_index = self.color_index-self.num_colors
                alternate = True
                index = np.linspace(0.0, common.kNumLEDs-1, common.kNumLEDs)
                mask = index%2 == 0
            else:
                local_index = self.color_index
                alternate = False
                mask = np.ones(common.kNumLEDs, dtype=bool)

            if self.color_index == 0:
                prog_color = [1.0, 1.0, 1.0]
            else:
                prog_color = color.gradient(self.color_values[local_index-1], 'rainbow')

            options = {}
            options['color'] = prog_color
            options['end_after_lifetime'] = False
            options['time_fadein'] = 1.0
            options['cycles_active'] = 1
            options['cycles_inactive'] = 0
            options['enabled'] = mask
            prog_def = (ProgramStrobeLight, options)
            self.garden.startIdleProgram(prog_def)

            if alternate:
                options = {}
                options['color'] = [1.0, 1.0, 1.0]
                options['end_after_lifetime'] = False
                options['time_fadein'] = 1.0
                options['enabled'] = ~mask
                prog_def = (ProgramColor, options)
                self.garden.startIdleProgram(prog_def)

        if ii == 2:
            self.garden.decreaseSpeed()


class ModeBubbler(LEDGardenMode):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Define background programs.
        self.background_programs = [
            (ProgramBubbleLauncher, None)
            ]

        # Define idle programs.
        #self.idle_programs = [
        #    (ProgramBubbleLauncher, None)
        #    ]

        options = {'color_scheme':'blue'}
        self.button_programs[0] = [
            (ProgramBubbleBurst, options)
            ]
        options = {'color_scheme':'green'}
        self.button_programs[1] = [
            (ProgramBubbleBurst, options)
            ]
        options = {'color_scheme':'red'}
        self.button_programs[2] = [
            (ProgramBubbleBurst, options)
            ]


class ModeSpotSender(LEDGardenMode):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Define background programs.
        self.background_programs = [
            (ProgramSparklyHotaru, None)
            ]

        # Define idle programs.
        self.idle_programs = [
            (ProgramHeartbeat, None)
            ]
        #self.idle_programs = []

        # Define button programs.
        self.button_programs = OrderedDict()

        options = {'end_after_lifetime':True
                   ,'time_lifetime':15.0
                   ,'color':np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)}
        self.button_programs[0] = [
            (ProgramTravelingSpotRandom, options)
            ]

        options = {'end_after_lifetime':True
                   ,'time_lifetime':15.0
                   ,'color':np.array([0.0, 1.0, 0.0], dtype=common.k_default_dtype)}
        self.button_programs[1] = [
            (ProgramTravelingSpotRandom, options)
            ]

        options = {'end_after_lifetime':True
                   ,'time_lifetime':15.0
                   ,'color':np.array([0.0, 0.0, 1.0], dtype=common.k_default_dtype)}
        self.button_programs[2] = [
            (ProgramTravelingSpotRandom, options)
            ]

        self.button_programs[3] = [
            (ProgramTravelingSpotRandom, options)
            ]
        self.button_programs[4] = [
            (ProgramTravelingSpotRandom, options)
            ]


class ModeRainbowBurst(LEDGardenMode):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.gradient_names = [
            'rainbow cycle'
            ,'rastaman'
            ,'miami sunset'
            ,'fire'
            ,'aquarius'
            ,'seaweed'
            ,'mellow yellow'
            ,'wavy davey'
            ]

        self.gradient_values = [0.00, 0.17, 0.25, 0.33, 0.42, 0.58, 0.66, 0.83]
        self.color_index = -1
        self.gradient_index = 0


        options = {}
        #options['time_lifetime'] = 10.0
        self.idle_programs = [
            (ProgramRainbowScroll, options)
            ]


    def actionButtonPress(self, ii):

        if ii == 0:
            print('Launching a new color')
            self.color_index += 1
            if self.color_index >= len(self.gradient_values):
                self.color_index = 0

            prog_color = color.gradient(self.gradient_values[self.color_index], self.gradient_names[self.gradient_index])
            options = {}
            options['color'] = prog_color
            prog_def = (ProgramTravellingSpotBurst, options)
            self.garden.startActionProgram(prog_def)


        if ii == 1:
            self.garden.deactivateIdlePrograms(fadeout_time=1.0)

            self.gradient_index += 1
            if self.gradient_index >= len(self.gradient_names):
                self.gradient_index = 0
                self.garden.toggleMirrorMode()

            options = {}
            options['gradient'] = self.gradient_names[self.gradient_index]
            options['end_after_lifetime'] = False
            options['time_fadein'] = 1.01
            prog_def = (ProgramRainbowScroll, options)

            self.garden.startIdleProgram(prog_def)


        if ii == 2:

            options = {}
            options['gradient'] = self.gradient_names[self.gradient_index]
            options['end_after_lifetime'] = True
            options['time_lifetime'] = 10.0
            prog_def = (ProgramGradientBarPulser, options)
            self.garden.startActionProgram(prog_def)


class ModeFire(LEDGardenMode):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.gradient_names = [
            'rainbow cycle'
            ,'rastaman'
            ,'miami sunset'
            ,'fire'
            ,'aquarius'
            ,'seaweed'
            ,'mellow yellow'
            ,'wavy davey'
            ]

        self.gradient_index = 0

        mask = np.zeros(common.kNumLEDs, dtype=bool)

        # This is the idle program, but we have to treat it manually for this mode.
        options = {}
        options['size_max_range'] = [common.kNumLEDs/10.0, common.kNumLEDs/4.0]
        options['size_min_range'] = [0.0, common.kNumLEDs/6.0]
        options['gradient'] = 'fire'
        options['time_fadeout'] = 1.0
        self.other_programs.append((ProgramFire, options))


        # Start up two more idle programs, but disable them.
        options = {}
        options['gradient'] = 'fire'
        options['size_max_range'] = [0.0, 0.0]
        options['size_min_range'] = [0.0, 0.0]
        options['uptime_range'] = [0.2, 1.0]
        options['downtime_range'] = [0.2, 0.5]
        options['end_after_lifetime'] = False
        options['time_fadein'] = 0.5
        self.other_programs.append((ProgramButtonControledFire, options))


        options = {}
        options['gradient'] = 'aquarius'
        options['size_max_range'] = [0.0, 0.0]
        options['size_min_range'] = [0.0, 0.0]
        options['uptime_range'] = [0.5, 1.5]
        options['downtime_range'] = [0.5, 1.0]
        options['end_after_lifetime'] = False
        options['time_fadein'] = 0.5
        self.other_programs.append((ProgramButtonControledFire, options))


        options = {}
        options['end_after_lifetime'] = True
        options['time_lifetime'] = 10.0
        options['random_gradient'] = True
        self.button_programs[1] = [
            (ProgramFire, options)
            ]


    def activate(self):
        self.startOtherPrograms()

        # Start the idle program
        prog = self.garden.other_programs[0]
        prog.actionStartFadein()


    def startOtherPrograms(self):
        for prog in self.other_programs:
            self.garden.startOtherProgram(prog, fadein=False)


    def buttonStatusHandler(self):
        """
        A custom button handler for this mode.
        """
        #self.log.debug('Starting the button handler.')

        press_status = self.checkForButtonPush()
        handled = self.buttonCommandsHandler(press_status
                                             ,enable_speed=False
                                             ,enable_brightess=False)

        # Handle the center button press.
        if not handled:
            if press_status[1]:
                # Fade out the idle program(s).
                prog = self.garden.other_programs[0]
                prog.actionStartFadeout()
                prog.time_fadein_start = common.runtimeCycle() + 10.0
                print(prog.time_fadein_start)

                self.log.debug('Button {} was pressed.'.format(1))
                self.actionButtonPress(1)
                handled = True


        if not handled:

            if (self.garden.button_state[0]['state_current']
                or self.garden.button_state[2]['state_current']):

                # Fade out the idle program(s).
                prog = self.garden.other_programs[0]
                prog.actionStartFadeout()
                prog.time_fadein_start = common.runtimeCycle() + 10.0


            # Ok, now start the pressed button actions.
            if self.garden.button_state[0]['state_current']:

                # Find the mellow yellow program
                prog = self.garden.other_programs[1]
                prog.actionStartFadein()
                prog.time_fadeout_start = common.runtimeCycle() + 10.0
                prog.increaseHeight()


            if self.garden.button_state[2]['state_current']:

                # Find the mellow yellow program
                prog = self.garden.other_programs[2]
                prog.actionStartFadein()
                prog.time_fadeout_start = common.runtimeCycle() + 10.0
                prog.increaseHeight()