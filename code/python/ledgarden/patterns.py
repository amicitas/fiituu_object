
from collections import OrderedDict
import numpy as np

from mirutil import logging
from . import common
from . import color

class LEDGardenPattern:
    """
    A base definition for a pattern.
    This will both add and scale the LED values.
    """
    
    def __init__(self, led_state, options=None):
        self.log = logging.getLogger(self.__class__.__name__)

        self.log.info("Initializing {}.".format(self.__class__.__name__))
        self.led_state = led_state
        self.scale = 1.0
        self.current_cycle = 0

        self.flag_scale_led_values = True
        self.flag_add_led_values = True

        self.is_finished = False

        self.time_init = common.runtimeCycle()

        self.pattern_state = self.getNewPatternState()
        self.pattern_options = self.defaultPatternOptions()
        self.pattern_param = self.defaultPatternParam()

        self.setUserOptions(options)


    def getNewPatternState(self):
        
        # Save the color as an float array for now.
        color = np.zeros((common.kNumLEDs, 3), dtype=common.k_default_dtype)
        scale = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)
        mask = np.ones(common.kNumLEDs, dtype=bool)

        return {'enabled':mask
                ,'color':color
                ,'scale':scale}


    def defaultPatternOptions(self):
        """
        To be reimplemented.
        """
        options = {}
        options['time_start'] = self.time_init
        options['time_fadein'] = 0.0
        return options


    def defaultPatternParam(self):
        """
        To be reimplemented.
        """
        return {}


    def setUserOptions(self, options=None):
        if options:
            self.pattern_options.update(options)

            # Check some individual options.
            if 'color' in options:
                options['color'] = np.array(options['color'], dtype=common.k_default_dtype)

    def run(self):
        """
        The main loop routine.
        """

        # Don't do anything if the pattern hasn't started yet.
        if common.runtimeCycle() < self.pattern_options['time_start']:
            return

        #print('Running: {}'.format(self.__class__.__name__))
        self.setLEDPatternParam()

        # Add brightess to the LED value.
        self.addLEDValue(self.led_state, self.pattern_state)

        # Scale LED brightness.
        self.scaleLEDValue(self.led_state, self.pattern_state)
            
        # Check the min and max LED value values.
        # This important so that other programs can scale appropriatly
        # from the LED max value.
        self.cleanupLEDValue()

        self.current_cycle += 1


    def getScale(self):
        scale = 1.0

        # Handle pattern fadein.
        if self.pattern_options['time_fadein'] > 0:
            if (common.runtimeCycle() - self.pattern_options['time_start']) < self.pattern_options['time_fadein']:
                scale = (common.runtimeCycle() - self.pattern_options['time_start'])/self.pattern_options['time_fadein']

        scale *= self.scale
        return scale


    def addLEDValue(self, led_state, pattern_state):
        if self.flag_add_led_values:
            led_state['color'][self.pattern_state['enabled']] += \
                pattern_state['color'][self.pattern_state['enabled']] * common.kLEDMaxValue * self.getScale()


    def scaleLEDValue(self, led_state, pattern_state):
        if self.flag_scale_led_values:
            for ii in range(common.kLEDNumColors):
                led_state['color'][self.pattern_state['enabled'],ii] *= pattern_state['scale'][self.pattern_state['enabled']]


    def cleanupLEDValue(self):
        mask = self.led_state['color'] < common.kLEDMinValue
        self.led_state['color'][mask] = common.kLEDMinValue

        mask = self.led_state['color'] > common.kLEDMaxValue
        self.led_state['color'][mask] = common.kLEDMaxValue


    def disableLEDs(self, mask=None):
        if mask is not None:
            self.pattern_state['enabled'][mask] = False
        else:
            self.pattern_state['enabled'][:] = False


    def enableLEDs(self, mask=None):
        if mask is not None:
            self.pattern_state['enabled'][mask] = True
        else:
            self.pattern_state['enabled'][:] = True


    def setEnabled(self, mask):
        self.pattern_state['enabled'][:] = False
        self.pattern_state['enabled'][mask] = True


    def setLEDPatternParam(self):
        """
        Must be reimplimented by each pattern.

        This method will be called once each cycle and it is
        where each pattern should do all its work.
        """
        pass


    def isFinished(self):
        """
        Return the finished status of a pattern.
        """
        return self.is_finished


class LEDGardenPatternAdd(LEDGardenPattern):
    """
    An pattern that only addes to the LED values.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.flag_scale_led_values = False
        self.flag_add_led_value = True


class LEDGardenPatternScale(LEDGardenPattern):
    """
    An pattern that only scales the LED values.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.flag_scale_led_values = True
        self.flag_add_led_value = False


class PatternColorGradient(LEDGardenPatternAdd):
    """An always on pattern.  This sets all leds to the given color"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()
        options['gradient'] = 'rainbow cycle'

        return options


    def setLEDPatternParam(self):
        values = np.linspace(0.0, 1.0, common.kNumLEDs)
        colors = color.gradient(values, self.pattern_options['gradient'])
        self.pattern_state['color'][:, :] = colors


class PatternGradientScroll(PatternColorGradient):


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.startNewScroll()

        self.calculateInitTimeStart()


    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['gradient_speed'] = 30.0
        options['gradient_location_start'] = 0.0
        options['gradient_location_min'] = 0
        options['gradient_location_max'] = common.kNumLEDs-1

        return options


    def defaultPatternParam(self):
        param = super().defaultPatternParam()

        param['time_start'] = 0.0
        param['time_last'] = 0.0
        param['gradient_location'] = 0.0

        return param


    def calculateInitTimeStart(self):
        """
        Calculate a starting time that provides the requested starting location.
        """

        speed = self.pattern_options['gradient_speed'] * common.getSpeedFactor()

        if speed >= 0:
            bound = self.pattern_options['gradient_location_min']
        else:
            bound = self.pattern_options['gradient_location_max']

        # This only makes sense if speed is a finite value.
        if abs(speed) > 0:
            self.pattern_param['time_start'] = (
                common.runtimeCycle()
                - ((self.pattern_options['gradient_location_start'] - bound)
                / speed))


    def startNewScroll(self):
        self.pattern_param['time_start'] = common.runtimeCycle()
        self.pattern_param['time_last'] = self.pattern_param['time_start']
        if self.pattern_options['gradient_speed'] >= 0:
            self.pattern_param['gradient_location'] = self.pattern_options['gradient_location_min']
        else:
            self.pattern_param['gradient_location'] = self.pattern_options['gradient_location_max']


    def getSpeed(self):
        """Return the current speed of the spot."""
        return self.pattern_options['gradient_speed'] * common.getSpeedFactor()


    def setLEDPatternParam(self):


        speed = self.getSpeed()

        if speed >= 0:
            bound = self.pattern_options['gradient_location_min']
        else:
            bound = self.pattern_options['gradient_location_max']

        time_diff = common.runtimeCycle() - self.pattern_param['time_last']
        self.pattern_param['gradient_location'] += (time_diff * speed)

        if ((self.pattern_param['gradient_location'] > self.pattern_options['gradient_location_max'])
            or (self.pattern_param['gradient_location'] < self.pattern_options['gradient_location_min'])):

            self.startNewScroll()

        values = np.linspace(0.0, 1.0, (self.pattern_options['gradient_location_max'] - self.pattern_options['gradient_location_min'])+1)
        colors = color.gradient(values, self.pattern_options['gradient'])

        # Calculate a sub pixel shift.
        shift = np.int(np.floor(self.pattern_param['gradient_location'] - self.pattern_options['gradient_location_min']))
        fraction = (self.pattern_param['gradient_location'] - self.pattern_options['gradient_location_min'])%1.0
        for ii in range(common.kLEDNumColors):
            colors[:, ii] = np.roll(colors[:, ii], shift) * (1.0 - fraction) + np.roll(colors[:, ii], shift+1) * fraction


        self.pattern_state['color'][self.pattern_options['gradient_location_min']:self.pattern_options['gradient_location_max']+1, :] = colors


        self.pattern_param['time_last'] = common.runtimeCycle()


class PatternScale(LEDGardenPattern):
    """
    An constant scale pattern.
    This sets the LED scale to the given value (default 1.0).
    """

    def defaultPatternOptions(self):
        return {'scale':0.5}


    def setLEDPatternParam(self):
        self.pattern_state['scale'][:] = self.pattern_options['scale']


class PatternColor(LEDGardenPattern):
    """An always on pattern.  This sets all leds to a rainbow color."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()
        options['color'] = np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
        return options


    def setLEDPatternParam(self):
        for ii in range(common.kLEDNumColors):
            self.pattern_state['color'][:,ii] = self.pattern_options['color'][ii]


class PatternHeartbeat(LEDGardenPattern):


    def __init__(self, led_state):
        super().__init__(led_state)

        # 3 is my favorite value.
        self.heartbeat_period = 30

        self.heartbeat_param = self.getDefaultHeartbeatParam()


    def getDefaultHeartbeatParam(self):

        param = {}
        param['last_part'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype) - 1
        param['min_effect'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 0.2
        param['max_effect'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 1.0
        param['starting_value'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)

        return param


    def setLEDPatternParam(self):
        """
        This is the main routine to set the LED brightness for the heartbeat pattern.
        
        This is meant to give a nice visual impression of a heartbeat, rather than 
        neccessarily produce an accurate waveform.
        
        To start off I will split the heartbeat into 4 parts:
          1. Rise for first beat.
          2. Decay for first beat.
          3. Rise for second beat.
          4. Decay for second beat.
        """

        # This is good timing for a 'realistic' heart beat.
        #beat_timing = [0.1, 0.2, 0.3, 0.4, 0.9]

        # THis is better for a slow heard beat.
        beat_timing = [0.0, 0.2, 0.4, 0.6, 0.9]
        value = 0.0
        amplitude = 0.0

        part = 0

        # Calculate where in the period we are.
        period_length = self.heartbeat_period/common.getSpeedFactor()
        period_fraction = (common.runtimeCycle()%period_length)/period_length
        #print('runtime: {}'.format(common.runtimeCycle()))
        #print('period_fraction: {}'.format(period_fraction))
        
        # Figure out which part this corresponds to.
        part = -1
        for jj in range(0,4):
            if (period_fraction >= beat_timing[jj] and period_fraction < beat_timing[jj+1]):
                part = jj


        # If we just started a new part, then save the last value.
        # We use this to ensure smooth transisisons between parts.
        mask = part != self.heartbeat_param['last_part']
        self.heartbeat_param['starting_value'][mask] = self.pattern_state['scale'][mask]

        if (part == -1):
            # Outside of the programmed range.
            self.pattern_state['scale'][:] = self.heartbeat_param['min_effect'][:]
        else:
            period_dummy = ( (period_fraction - beat_timing[part])
                             /(beat_timing[part+1] - beat_timing[part])
                             * np.pi )

            if (part == 0 or part == 2):
                # Rising part.
                period_dummy += np.pi

                amplitude = (self.heartbeat_param['max_effect']
                             - self.heartbeat_param['starting_value'])
                value = ((np.cos(period_dummy)+1.0)/2.0 * amplitude 
                         + self.heartbeat_param['starting_value'])
            else:
                # Falling part
                amplitude = (self.heartbeat_param['starting_value']
                             - self.heartbeat_param['min_effect'])
                value = ((np.cos(period_dummy)+1.0)/2.0 * amplitude 
                         + self.heartbeat_param['min_effect'])

            self.pattern_state['scale'][:] = value

        self.heartbeat_param['last_part'][:] = part


class PatternHeartbeatColor(LEDGardenPattern):
    """
    Pulse all of the LEDs with a heart beat pattern.

    ToDo:
      This pattern does not need individual patters for each LED.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # 3 is my favorite value.
        self.heartbeat_period = 3.0

        self.heartbeat_param = self.getNewHeartbeatParam()

    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['color'] = np.array([1.00, 0.0, 0.0], dtype=common.k_default_dtype)

        return options

    def getNewHeartbeatParam(self):

        param = {}
        param['last_part'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype) - 1
        param['last_value'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        param['min_effect'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 0.0
        param['max_effect'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 0.8
        param['starting_value'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)

        return param


    def setLEDPatternParam(self):
        """
        This is the main routine to set the LED brightness for the heartbeat pattern.

        This is meant to give a nice visual impression of a heartbeat, rather than
        neccessarily produce an accurate waveform.

        To start off I will split the heartbeat into 4 parts:
          1. Rise for first beat.
          2. Decay for first beat.
          3. Rise for second beat.
          4. Decay for second beat.
        """

        beat_timing = [0.1, 0.2, 0.3, 0.4, 0.9]
        value = 0.0
        amplitude = 0.0

        part = 0

        # Calculate where in the period we are.
        period_length = self.heartbeat_period/common.getSpeedFactor()
        period_fraction = (common.runtimeCycle()%period_length)/period_length
        #print('runtime: {}'.format(common.runtimeCycle()))
        #print('period_fraction: {}'.format(period_fraction))

        # Figure out which part this corresponds to.
        part = -1
        for jj in range(0,4):
            if (period_fraction >= beat_timing[jj] and period_fraction < beat_timing[jj+1]):
                part = jj
        #print('part: {}'.format(part))

        # If we just started a new part, then save the last value.
        # We use this to ensure smooth transisisons between parts.
        mask = part != self.heartbeat_param['last_part']
        self.heartbeat_param['starting_value'][mask] = self.heartbeat_param['last_value'][mask]

        if (part == -1):
            # Outside of the programmed range.
            value = self.heartbeat_param['min_effect'][:]
        else:
            period_dummy = ( (period_fraction - beat_timing[part])
                             /(beat_timing[part+1] - beat_timing[part])
                             * np.pi )

            if (part == 0 or part == 2):
                # Rising part.
                period_dummy += np.pi

                amplitude = (self.heartbeat_param['max_effect']
                             - self.heartbeat_param['starting_value'])
                value = ((np.cos(period_dummy)+1.0)/2.0 * amplitude
                         + self.heartbeat_param['starting_value'])
            else:
                # Falling part
                amplitude = (self.heartbeat_param['starting_value']
                             - self.heartbeat_param['min_effect'])
                value = ((np.cos(period_dummy)+1.0)/2.0 * amplitude
                         + self.heartbeat_param['min_effect'])

            #print('Value: {}'.format(value))
        for ii in range(common.kLEDNumColors):
            self.pattern_state['color'][:,ii] = self.pattern_options['color'][ii] * value

        self.heartbeat_param['last_value'][:] = value
        self.heartbeat_param['last_part'][:] = part


class PatternTwinkleOff(LEDGardenPattern):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.twinkle_param = self.getDefaultTwinkleParam()

        for ii in range(common.kNumLEDs):
            self.newTwinkleParam()


    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()
        options['fraction_active'] = 0.5
        options['min_effect'] = 0.5
        options['max_effect'] = 1.2
        options['min_period'] = 0.2
        options['max_period'] = 1.2
        options['period_repeat'] = 2

        return options


    def getDefaultTwinkleParam(self):

        param = {}
        param['min_effect'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 0.5
        param['max_effect'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 1.2
        param['period'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        param['time_start'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)

        return param


    def setLEDPatternParam(self):

        # Calculate a brightness factor and multiply the brightness state.
        self.pattern_state['scale'] = (
            ( np.sin(2.0*np.pi * (common.runtimeCycle()
                                     - self.twinkle_param['time_start'])
                       / self.twinkle_param['period']) / 2.0+0.5)
            * (self.twinkle_param['max_effect'] - self.twinkle_param['min_effect'])
            + self.twinkle_param['min_effect'])

        # To achive a randomized twinkle pattern, 
        # I reset the twinkle parameters every x periods.
        need_new = (
            ((common.runtimeCycle()-self.twinkle_param['time_start'])
            > (self.twinkle_param['period'] * self.pattern_options['period_repeat'])))

        self.newTwinkleParam(need_new)


    def newTwinkleParam(self, mask=None):
        if mask is None:
            mask = np.ones(common.kNumLEDs, dtype=bool)
        num_new = np.count_nonzero(mask)

        self.twinkle_param['period'][mask] = np.random.uniform(self.pattern_options['min_period'], self.pattern_options['max_period'], num_new)
        self.twinkle_param['time_start'][mask] = common.runtimeCycle()
        self.twinkle_param['min_effect'][mask] = self.pattern_options['min_effect']
        self.twinkle_param['max_effect'][mask] = self.pattern_options['max_effect']

        # Deactivate the requested fraction of LEDs.
        filter_values = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        filter_values[mask] = np.random.random(num_new)
        filter_mask = filter_values > self.pattern_options['fraction_active']
        self.twinkle_param['min_effect'][filter_mask] = 1.0
        self.twinkle_param['max_effect'][filter_mask] = 1.0
        self.twinkle_param['period'][filter_mask] = self.pattern_options['max_period']


class PatternTwinkleColor(LEDGardenPattern):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.twinkle_param = self.getDefaultTwinkleParam()

        self.newTwinkleParam()


    def defaultPatternOptions(self):

        options = super().defaultPatternOptions()
        options['fraction_active'] = 0.2
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        options['min_effect'] = 0.2
        options['max_effect'] = 1.0
        options['min_period'] = 0.2
        options['max_period'] = 1.2
        options['period_repeat'] = 2

        return options

    def getDefaultTwinkleParam(self):

        param = {}
        param['min_effect'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 0.5
        param['max_effect'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 1.2
        param['period'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        param['time_start'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)

        return param


    def setLEDPatternParam(self):

        # Calculate a brightness factor and multiply the brightness state.
        scale = (
            self.twinkle_param['min_effect'] + (
                ( np.sin(2.0*np.pi * (common.runtimeCycle()
                        - self.twinkle_param['time_start'])
                    / self.twinkle_param['period']) / 2.0+0.5)
                * (self.twinkle_param['max_effect'] - self.twinkle_param['min_effect'])))

        for ii in range(common.kLEDNumColors):
            self.pattern_state['color'][:,ii] = self.pattern_options['color'][ii] * scale

        # To achieve a randomized twinkle pattern,
        # I reset the twinkle parameters every x periods.
        need_new = (
            ((common.runtimeCycle()-self.twinkle_param['time_start'])
            > (self.twinkle_param['period'] * self.pattern_options['period_repeat'])))

        self.newTwinkleParam(need_new)


    def newTwinkleParam(self, mask=None):
        if mask is None:
            mask = np.ones(common.kNumLEDs, dtype=bool)
        num_new = np.count_nonzero(mask)

        self.twinkle_param['period'][mask] = np.random.uniform(self.pattern_options['min_period'], self.pattern_options['max_period'], num_new)
        self.twinkle_param['time_start'][mask] = common.runtimeCycle()

        self.twinkle_param['min_effect'][mask] = self.pattern_options['min_effect']
        self.twinkle_param['max_effect'][mask] = np.random.uniform(self.pattern_options['min_effect'], self.pattern_options['max_effect'], num_new)

        # Deactivate the requested fraction of LEDs.
        filter_values = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        filter_values[mask] = np.random.random(num_new)
        filter_mask = filter_values > self.pattern_options['fraction_active']
        self.twinkle_param['min_effect'][filter_mask] = 0.0
        self.twinkle_param['max_effect'][filter_mask] = 0.0
        self.twinkle_param['period'][filter_mask] = self.pattern_options['max_period']


class PatternPulse(LEDGardenPattern):


    def __init__(self, led_state, options=None):
        super().__init__(led_state, options=options)

        # Setup default parameters.
        self.up_time_range = [0.5, 2.0]
        self.down_time_range = [2.0, 4.0]
        self.off_time_range = [0.0, 1.0]
        self.max_value_range = [1.0, 1.0]


        self.pattern_param = self.getDefaultPatternParam()

        self.newPatternParam()
        self.initNewPhase()
            

    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()
        options['fraction_active'] = 0.7
        return options


    def getDefaultPatternParam(self):

        param = {}
        param['max_value'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)
        param['up_time'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)
        param['down_time'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)
        param['off_time'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 0.5

        param['phase_start_value'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        param['phase'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        param['phase_length'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        param['phase_time_start'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)

        return param


    def setUpTimeRange(self, min, max):
        self.up_time_range[0] = min
        self.up_time_range[1] = max
            

    def setDownTimeRange(self, min, max):
        self.down_time_range[0] = min
        self.down_time_range[1] = max


    def setOffTimeRange(self,min, max):
        self.off_time_range[0] = min
        self.off_time_range[1] = max


    def setMaxValueRange(self,min, max):
        self.max_value_range[0] = min
        self.max_value_range[1] = max



    def getPhaseFraction(self):
        """Find where in the current phase we are."""

        # I should have a way to skip over zero length phases,
        # but for now this will have to do.
        phase_fraction = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)

        mask = self.pattern_param['phase_length'] > 0
        phase_fraction[mask] = ((common.runtimeCycle() - self.pattern_param['phase_time_start'][mask])
                                /self.pattern_param['phase_length'][mask])

        return phase_fraction


    def initNewPhase(self, mask=None):
        if mask is None:
            mask = np.ones(common.kNumLEDs, dtype=bool)

        phase_mask = self.pattern_param['phase'] == 0
        self.pattern_param['phase_length'][mask & phase_mask] = self.pattern_param['up_time'][mask & phase_mask]

        phase_mask = self.pattern_param['phase'] == 1
        self.pattern_param['phase_length'][mask & phase_mask] = self.pattern_param['down_time'][mask & phase_mask]

        phase_mask = self.pattern_param['phase'] == 2
        self.pattern_param['phase_length'][mask & phase_mask] = self.pattern_param['off_time'][mask & phase_mask]


        self.pattern_param['phase_time_start'][mask] = common.runtimeCycle()
        self.pattern_param['phase_start_value'][mask] = self.pattern_state['scale'][mask]


    def advanceToNextPhase(self, mask=None):
        if mask is None:
            mask = np.ones(common.kNumLEDs, dtype=bool)

        phase_mask = self.pattern_param['phase'] < 2
        self.pattern_param['phase'][mask & phase_mask] += 1

        self.pattern_param['phase'][mask & (~phase_mask)] = 0
        self.newPatternParam(mask & (~phase_mask))
        
        self.initNewPhase(mask)



    def setLEDPatternParam(self):

        phase_fraction = self.getPhaseFraction()
  
        # Check to see if we are done with the previous phase.
        # If so, switch to the next phase.
        mask = phase_fraction >= 1.0
        self.advanceToNextPhase(mask)
        phase_fraction[mask] = 0.0


        phase = self.pattern_param['phase']

        theta = phase_fraction * np.pi

        # Up phase.
        mask = self.pattern_param['phase'] == 0
        theta += np.pi
        self.pattern_state['scale'][mask] = (
            (np.cos(theta[mask])+1.0)/2.0
            * (self.pattern_param['max_value'][mask] - self.pattern_param['phase_start_value'][mask])
            + self.pattern_param['phase_start_value'][mask])

        # Down phase.
        mask = self.pattern_param['phase'] == 1
        self.pattern_state['scale'][mask] = (np.cos(theta[mask])+1.0)/2.0 * self.pattern_param['phase_start_value'][mask]

        # Off phase
        mask = self.pattern_param['phase'] >= 2
        self.pattern_state['scale'][mask] = 0


    def newPatternParam(self, mask=None):
        if mask is None:
            mask = np.ones(common.kNumLEDs, dtype=bool)
        num_new = np.count_nonzero(mask)

        self.pattern_param['max_value'][mask] = np.random.uniform(self.max_value_range[0], self.max_value_range[1], num_new)
        self.pattern_param['up_time'][mask] = np.random.uniform(self.up_time_range[0], self.up_time_range[1], num_new)
        self.pattern_param['down_time'][mask] = np.random.uniform(self.down_time_range[0], self.down_time_range[1], num_new)
        self.pattern_param['off_time'][mask] = np.random.uniform(self.off_time_range[0], self.off_time_range[1], num_new)

        # Deactivate the requested fraction of LEDs.
        filter_values = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        filter_values[mask] = np.random.random(num_new)
        filter_mask = filter_values > self.pattern_options['fraction_active']
        self.pattern_param['max_value'][filter_mask] = 0.0


class PatternHotaru(LEDGardenPattern):

    def __init__(self, led_state, options=None):
        super().__init__(led_state, options=options)

        # Setup default parameters.
        self.off_time_range = [1.0, 6.0]
        self.up_time_range = [1.0, 2.0]
        self.on_time_range = [0.0, 4.0]
        self.down_time_range = [0.5, 1.0]
        self.max_value_range = [1.0, 1.0]

        self.pattern_param = self.getDefaultPatternParam()

        self.newPatternParam()
        self.initNewPhase()


    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()
        options['fraction_active'] = 0.1
        return options


    def getDefaultPatternParam(self):

        param = {}
        param['max_value'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)
        param['up_time'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)
        param['on_time'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)
        param['down_time'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)
        param['off_time'] = np.ones(common.kNumLEDs, dtype=common.k_default_dtype) * 0.5

        param['phase_start_value'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        param['phase'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        param['phase_length'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        param['phase_time_start'] = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)

        return param


    def setUpTimeRange(self, min, max):
        self.up_time_range[0] = min
        self.up_time_range[1] = max
            
    def setDownTimeRange(self, min, max):
        self.down_time_range[0] = min
        self.down_time_range[1] = max

    def setOffTimeRange(self,min, max):
        self.off_time_range[0] = min
        self.off_time_range[1] = max

    def setOnTimeRange(self,min, max):
        self.on_time_range[0] = min
        self.on_time_range[1] = max

    def setMaxValueRange(self,min, max):
        self.max_value_range[0] = min
        self.max_value_range[1] = max


    def getPhaseFraction(self):
        """Find where in the current phase we are."""

        # I should have a way to skip over zero length phases,
        # but for now this will have to do.
        phase_fraction = np.ones(common.kNumLEDs, dtype=common.k_default_dtype)

        mask = self.pattern_param['phase_length'] > 0
        phase_fraction = ((common.runtimeCycle() - self.pattern_param['phase_time_start'])
                              /self.pattern_param['phase_length'])

        return phase_fraction


    def initNewPhase(self, mask=None):
        if mask is None:
            mask = np.ones(common.kNumLEDs, dtype=bool)

        phase_mask = self.pattern_param['phase'] == 0
        self.pattern_param['phase_length'][mask & phase_mask] = self.pattern_param['off_time'][mask & phase_mask]

        phase_mask = self.pattern_param['phase'] == 1
        self.pattern_param['phase_length'][mask & phase_mask] = self.pattern_param['up_time'][mask & phase_mask]

        phase_mask = self.pattern_param['phase'] == 2
        self.pattern_param['phase_length'][mask & phase_mask] = self.pattern_param['on_time'][mask & phase_mask]

        phase_mask = self.pattern_param['phase'] == 3
        self.pattern_param['phase_length'][mask & phase_mask] = self.pattern_param['down_time'][mask & phase_mask]


        self.pattern_param['phase_time_start'][mask] = common.runtimeCycle()
        self.pattern_param['phase_start_value'][mask] = self.pattern_state['scale'][mask]


    def advanceToNextPhase(self, mask=None):
        if mask is None:
            mask = np.ones(common.kNumLEDs, dtype=bool)


        phase_mask = self.pattern_param['phase'] < 3
        self.pattern_param['phase'][mask & phase_mask] += 1

        self.pattern_param['phase'][mask & (~phase_mask)] = 0
        self.newPatternParam(mask & (~phase_mask))

        self.initNewPhase(mask)


    def newPatternParam(self, mask=None):
        if mask is None:
            mask = np.ones(common.kNumLEDs, dtype=bool)
        num_new = np.count_nonzero(mask)

        self.pattern_param['max_value'][mask] = np.random.uniform(self.max_value_range[0], self.max_value_range[1], num_new)
        self.pattern_param['off_time'][mask] = np.random.uniform(self.off_time_range[0], self.off_time_range[1], num_new)
        self.pattern_param['up_time'][mask] = np.random.uniform(self.up_time_range[0], self.up_time_range[1], num_new)
        self.pattern_param['on_time'][mask] = np.random.uniform(self.on_time_range[0], self.on_time_range[1], num_new)
        self.pattern_param['down_time'][mask] = np.random.uniform(self.down_time_range[0], self.down_time_range[1], num_new)

        # Deactivate the requested fraction of LEDs.
        filter_values = np.zeros(common.kNumLEDs, dtype=common.k_default_dtype)
        filter_values[mask] = np.random.random(num_new)
        filter_mask = filter_values > self.pattern_options['fraction_active']
        self.pattern_param['max_value'][filter_mask] = 0.0


    def setLEDPatternParam(self):

        phase_fraction = self.getPhaseFraction()

        # Check to see if we are done with the previous phase.
        # If so, switch to the next phase.
        mask = phase_fraction >= 1.0
        self.advanceToNextPhase(mask)
        phase_fraction[mask] = 0.0


        phase = self.pattern_param['phase']

        theta = phase_fraction * np.pi

        #print(self.pattern_param)
        #print('Phase: ', self.pattern_param['phase'])

        # Off phase
        mask = self.pattern_param['phase'] == 0
        self.pattern_state['scale'][mask] = 0

        # Up phase.
        mask = self.pattern_param['phase'] == 1
        theta[mask] += np.pi
        self.pattern_state['scale'][mask] = (
            (np.cos(theta[mask])+1.0)/2.0
            * (self.pattern_param['max_value'][mask] - self.pattern_param['phase_start_value'][mask])
            + self.pattern_param['phase_start_value'][mask])

        # On phase.
        mask = self.pattern_param['phase'] == 2
        theta[mask] += np.pi
        self.pattern_state['scale'][mask] = self.pattern_param['phase_start_value'][mask]


        # Down phase.
        mask = self.pattern_param['phase'] == 3
        self.pattern_state['scale'][mask] = (np.cos(theta[mask])+1.0)/2.0 * self.pattern_param['phase_start_value'][mask]


class PatternHotaruColor(PatternHotaru):

    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['color'] = np.array([0.85, 1.0, 0.0], dtype=common.k_default_dtype)

        return options


    def scaleLEDValue(self, led_state, pattern_state):
        # Disable standard scaling for this pattern, but still apply the input scale value.
        led_state['color'][:,:] *= self.scale


    def setLEDPatternParam(self):
        super().setLEDPatternParam()

        #print(self.pattern_state['scale'])
        for ii in range(common.kLEDNumColors):
            self.pattern_state['color'][:,ii] = self.pattern_options['color'][ii] * self.pattern_state['scale']


class PatternGaussianSpot(LEDGardenPatternAdd):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.index = np.linspace(0.0, common.kNumLEDs-1, common.kNumLEDs)

    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['location'] = 0.0
        options['width'] = 3.0
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)

        options['location_min'] = 0
        options['location_max'] = common.kNumLEDs-1
        options['wrap_around'] = False

        return options


    def gaussian(self, x, amp, loc, width):
        #print('amp={}, loc={}, width={}'.format(amp, loc, width))
        return float(amp)*np.e**(-1*(x-float(loc))**2/(2*float(width)**2))


    def getSpotArray(self, location=None, width=None):

        if location is None: location = self.pattern_options['location']
        if width is None: width = self.pattern_options['width']

        array = self.gaussian(self.index, 1.0, location, width)

        if self.pattern_options['wrap_around']:
            if location < self.pattern_options['location_min']+width*4:
                wrap = self.gaussian(self.index, 1.0, location+self.pattern_options['location_max'], width)
                array[:] += wrap
            if location > self.pattern_options['location_max']-width*4:
                wrap = self.gaussian(self.index, 1.0, self.pattern_options['location_min'] - (self.pattern_options['location_max']-location), width)
                array[:] += wrap


        return array


    def setLEDPatternParam(self):

        array = self.getSpotArray()
        for ii in range(common.kLEDNumColors):
            self.pattern_state['color'][:,ii] = self.pattern_options['color'][ii]*array


class PatternSpotScroll(PatternGaussianSpot):


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.startNewScroll()

        self.calculateInitTimeStart()


    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['speed'] = 20.0
        options['location_start'] = options['location_min']

        # Set True by default.
        options['wrap_around'] = True

        # If True the pattern will end when the spot reaches a boundry.
        options['finish_at_bounds'] = False

        return options


    def defaultPatternParam(self):
        param = {}
        param['time_start'] = 0.0
        param['time_last'] = 0.0
        param['location'] = 0.0

        return param


    def calculateInitTimeStart(self):
        """
        Calculate a starting time that provides the requested starting location.
        """

        speed = self.pattern_options['speed'] * common.getSpeedFactor()

        if speed >= 0:
            bound = self.pattern_options['location_min']
        else:
            bound = self.pattern_options['location_max']

        # This only makes sense if speed is a finite value.
        if abs(speed) > 0:
            self.pattern_param['time_start'] = (
                common.runtimeCycle()
                - ((self.pattern_options['location_start'] - bound)
                / speed))


    def startNewScroll(self):
        self.pattern_param['time_start'] = common.runtimeCycle()
        self.pattern_param['time_last'] = self.pattern_param['time_start']
        if self.pattern_options['speed'] >= 0:
            self.pattern_param['location'] = self.pattern_options['location_min']
        else:
            self.pattern_param['location'] = self.pattern_options['location_max']


    def getSpeed(self):
        """Return the current speed of the spot."""
        return self.pattern_options['speed'] * common.getSpeedFactor()


    def setLEDPatternParam(self):

        speed = self.getSpeed()

        if speed >= 0:
            bound = self.pattern_options['location_min']
        else:
            bound = self.pattern_options['location_max']

        time_diff = common.runtimeCycle() - self.pattern_param['time_last']
        self.pattern_param['location'] += (time_diff * speed)

        if ((self.pattern_param['location'] > self.pattern_options['location_max'])
            or (self.pattern_param['location'] < self.pattern_options['location_min'])):

            if self.pattern_options['finish_at_bounds']:
                self.is_finished = True
            else:
                self.startNewScroll()

        array = self.getSpotArray(location=self.pattern_param['location'])
        for ii in range(common.kLEDNumColors):
            self.pattern_state['color'][:,ii] = self.pattern_options['color'][ii]*array

        self.pattern_param['time_last'] = common.runtimeCycle()


class PatternBubble(PatternSpotScroll):

    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['color'] = np.array([0.0, 0.75, 1.0], dtype=common.k_default_dtype)
        options['width'] = 1.0

        options['wrap_around'] = False
        options['speed_range'] = [1.0, 20.0]
        options['time_accelerate_range'] = [0.5, 2.0]
        options['time_constant_range'] = [0.0, 1.0]

        options['finish_at_bounds'] = True

        return options


    def defaultPatternParam(self):
        param = super().defaultPatternParam()

        param['phase_start_value'] = 0.0
        param['phase_end_value'] = 0.0
        param['phase_time_start'] = 0.0
        param['phase_length'] = 0.0
        param['phase'] = 1
        param['speed'] = 0.0

        return param


    def getSpeed(self):

        phase_fraction = self.getPhaseFraction()

        # Check to see if we are done with the previous phase.
        # If so, switch to the next phase.
        if phase_fraction >= 1.0:
            self.advanceToNextPhase()
            phase_fraction = 0.0

        theta = phase_fraction * np.pi

        # Acceleration Phase.
        if self.pattern_param['phase'] == 0:
            theta += np.pi
            self.pattern_param['speed'] = (
                (np.cos(theta)+1.0)/2.0
                * (self.pattern_param['phase_end_value'] - self.pattern_param['phase_start_value'])
                + self.pattern_param['phase_start_value'])
        # Constant speed phase.
        elif self.pattern_param['phase'] == 1:
            # Nothing to do here, speed stays constant.
            pass

        return self.pattern_param['speed']* common.getSpeedFactor()


    def getPhaseFraction(self):
        """Find where in the current phase we are."""

        if self.pattern_param['phase_length'] == 0.0:
            phase_fraction = 1.0
        else:
            phase_fraction = (
                (common.runtimeCycle() - self.pattern_param['phase_time_start'])
                /self.pattern_param['phase_length'])

        return phase_fraction


    def advanceToNextPhase(self):
        if self.pattern_param['phase'] == 0:
            self.pattern_param['phase'] += 1

        elif self.pattern_param['phase'] == 1:
            self.pattern_param['phase'] = 0

        self.initNewPhase()


    def initNewPhase(self):

        if self.pattern_param['phase'] == 0:
            # Acceleration phase
            self.pattern_param['phase_length'] = np.random.uniform(
                self.pattern_options['time_accelerate_range'][0]
                ,self.pattern_options['time_accelerate_range'][1])
            self.pattern_param['phase_end_value'] = np.random.uniform(
                self.pattern_options['speed_range'][0]
                ,self.pattern_options['speed_range'][1])

        elif self.pattern_param['phase'] == 1:
            # Constant speed phase.
            self.pattern_param['phase_length'] = np.random.uniform(
                self.pattern_options['time_constant_range'][0]
                ,self.pattern_options['time_constant_range'][0])

        self.pattern_param['phase_time_start'] = common.runtimeCycle()
        self.pattern_param['phase_start_value'] = self.pattern_param['speed']


class BasePatternStrobe(LEDGardenPattern):
    """
    The baseclass for a strobelight pattern.

    Note that this is a cycle based pattern instead of a time based pattern.
    This means that the speed that the strobe runs will be based on the
    cycle of the particular setup.

    Also this program handles the global speed parameter in a cycle based way.
    The speed paramatere will affect how many cycles the LEDS will be set to
    on or off.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.adjustPhase(self.pattern_options['phase_adjust'])


    def adjustPhase(self, phase):
        self.pattern_param['phase_start'] += phase


    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        options['cycles_active'] = 1
        options['cycles_inactive'] = 2
        options['scale_inactive_only'] = True
        options['phase_adjust'] = 0

        return options


    def defaultPatternParam(self):
        param = super().defaultPatternParam()

        param['cycle_count'] = -1
        param['phase_start'] = 0.0
        param['phase_number'] = 0.0
        param['state'] = 0.0
        param['phase_length'] = 0.0

        return param


    def setLEDPatternParam(self):

        self.pattern_param['cycle_count'] += 1

        if self.pattern_param['phase_number'] == 0:
            # Currently in the off phase.
            phase_length = self.pattern_options['cycles_inactive']
            phase_length -= common.options['speed']
            if phase_length < 1:
                phase_length = 1

            if self.pattern_param['cycle_count'] - self.pattern_param['phase_start'] >= phase_length:
                self.pattern_param['state'] = 1
                self.pattern_param['phase_number'] = 1
                self.pattern_param['phase_start'] = self.pattern_param['cycle_count']


        elif self.pattern_param['phase_number'] == 1:
            # Currently in the on phase.
            phase_length = self.pattern_options['cycles_active']
            if not self.pattern_options['scale_inactive_only']:
                phase_length -= common.options['speed']
            if phase_length < 1:
                phase_length = 1

            if self.pattern_param['cycle_count'] - self.pattern_param['phase_start'] >= phase_length:
                if self.pattern_options['cycles_inactive'] - common.options['speed'] > 0:
                    self.pattern_param['state'] = 0
                    self.pattern_param['phase_number'] = 0

                self.pattern_param['phase_start'] = self.pattern_param['cycle_count']


class PatternStrobeOn(BasePatternStrobe):
    """
    A pattern that turns on all enabled LED in a strobe pattern.
    """

    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        return options


    def setLEDPatternParam(self):
        super().setLEDPatternParam()

        if self.pattern_param['state'] == 0:
            self.pattern_state['color'][:,:] = 0.0
        else:
            for ii in range(common.kLEDNumColors):
                self.pattern_state['color'][:,ii] = self.pattern_options['color'][ii]


class PatternStrobeOff(BasePatternStrobe):
    """
    A pattern that turns off all enabled LEDs in a strobe pattern.
    """

    def setLEDPatternParam(self):
        super().setLEDPatternParam()

        if self.pattern_param['state'] == 0:
            self.pattern_state['scale'][:] = 0.0
        else:
            self.pattern_state['scale'][:] = 1.0


class BasePatternFractalStrobe(BasePatternStrobe):


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.index = np.linspace(0.0, common.kNumLEDs-1, common.kNumLEDs)


    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['time_doubling'] = 2.0
        options['reverse_segments'] = False

        return options


    def defaultPatternParam(self):
        param = super().defaultPatternParam()

        param['doubling_time_start'] = common.runtime()
        param['num_segments'] = 1

        return param


    def controlSegments(self):


        if common.runtimeCycle() - self.pattern_param['doubling_time_start'] >= self.pattern_options['time_doubling']:
            self.pattern_param['num_segments'] *= 2
            self.pattern_param['doubling_time_start'] = common.runtimeCycle()

            if self.pattern_options['reverse_segments']:
                starter = [True,False]
            else:
                starter = [False,True]

            # This is using integer math
            num_repeat = int(common.kNumLEDs/int(self.pattern_param['num_segments'])/2)
            if num_repeat < 1:
                num_repeat = 1
            num_tile = int(np.ceil(common.kNumLEDs/num_repeat/2))

            # This will create an oversized array.
            mask = np.tile(np.repeat(starter, num_repeat), num_tile)
            self.disableLEDs()
            self.enableLEDs(mask[0:common.kNumLEDs])


    def setLEDPatternParam(self):
        super().setLEDPatternParam()
        self.controlSegments()


class PatternFractalStrobeOn(BasePatternFractalStrobe):
    """
    A pattern that turns on half of the LEDs in a strobing fractal pattern.
    """

    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        return options


    def setLEDPatternParam(self):
        super().setLEDPatternParam()

        if self.pattern_param['state'] == 0:
            self.pattern_state['color'][:,:] = 0.0
        else:
            for ii in range(common.kLEDNumColors):
                self.pattern_state['color'][:,ii] = self.pattern_options['color'][ii]


class PatternFractalStrobeOff(BasePatternFractalStrobe):
    """
    A pattern that turns off half of the LEDs in a strobing fractal pattern.
    """

    def setLEDPatternParam(self):
        super().setLEDPatternParam()

        if self.pattern_param['state'] == 0:
            self.pattern_state['scale'][:] = 0.0
        else:
            self.pattern_state['scale'][:] = 1.0


class PatternExpandAndContract(LEDGardenPatternScale):
    """
    Expand and contract outward from a given location.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.index = np.linspace(0.0, common.kNumLEDs-1, common.kNumLEDs)


    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['size_max_range'] = [0.0, common.kNumLEDs-1]
        options['size_min_range'] = [0.0, common.kNumLEDs-1]

        options['location'] = 0.0

        options['uptime_range'] = [1.0, 1.0]
        options['downtime_range'] =  [0.5, 0.5]
        options['offtime_range'] =  [0.0, 0.0]
        options['ontime_range'] = [0.0, 0.0]

        options['edge_fadeout'] = True
        options['edge_size'] = 10

        options['end_after_upphase'] = False
        options['end_after_downphase'] = False

        return options


    def defaultPatternParam(self):
        param = super().defaultPatternParam()

        param['size'] = 0.0

        param['phase'] = -1
        param['phase_start_time'] = 0.0
        param['phase_start_value'] = 0.0
        param['phase_end_value'] = 0.0
        param['phase_length'] = 0.0

        return param


    def getPhaseFraction(self):
        """Find where in the current phase we are."""

        # Skip over zero length phases.
        if self.pattern_param['phase_length'] == 0.0:
            phase_fraction = 1.0
        else:
            phase_fraction = ((common.runtimeCycle() - self.pattern_param['phase_time_start'])
                              /self.pattern_param['phase_length'])

        return phase_fraction


    def advanceToNextPhase(self):

        # Check to see if the pattern is finished.
        if self.pattern_param['phase'] == 0 and self.pattern_options['end_after_upphase']:
            self.is_finished = True
        elif self.pattern_param['phase'] == 2 and self.pattern_options['end_after_downphase']:
            self.is_finished = True

        if self.pattern_param['phase'] < 3:
            self.pattern_param['phase'] += 1

        elif self.pattern_param['phase'] == 3:
            self.pattern_param['phase'] = 0

        self.initNewPhase()


    def initNewPhase(self):

        if self.pattern_param['phase'] == 0:
            # Up phase
            self.pattern_param['phase_length'] = np.random.uniform(
                self.pattern_options['uptime_range'][0]
                ,self.pattern_options['uptime_range'][1])
            self.pattern_param['phase_end_value'] = np.random.uniform(
                self.pattern_options['size_max_range'][0]
                ,self.pattern_options['size_max_range'][1])

        elif self.pattern_param['phase'] == 1:
            # On phase.
            self.pattern_param['phase_length'] = np.random.uniform(
                self.pattern_options['ontime_range'][0]
                ,self.pattern_options['ontime_range'][1])

            self.pattern_param['phase_end_value'] = self.pattern_param['size']

        elif self.pattern_param['phase'] == 2:
            # Down phase.
            self.pattern_param['phase_length'] = np.random.uniform(
                self.pattern_options['downtime_range'][0]
                ,self.pattern_options['downtime_range'][1])
            self.pattern_param['phase_end_value'] = np.random.uniform(
                self.pattern_options['size_min_range'][0]
                ,self.pattern_options['size_min_range'][1])

        elif self.pattern_param['phase'] == 3:
            # Off phase.
            self.pattern_param['phase_length'] = np.random.uniform(
                 self.pattern_options['offtime_range'][0]
                 ,self.pattern_options['offtime_range'][1])

            self.pattern_param['phase_end_value'] = self.pattern_param['size']

        self.pattern_param['phase_time_start'] = common.runtimeCycle()
        self.pattern_param['phase_start_value'] = self.pattern_param['size']


    def calculateSize(self):

        phase_fraction = self.getPhaseFraction()

        # Check to see if we are done with the previous phase.
        # If so, switch to the next phase.
        if phase_fraction >= 1.0:
            self.advanceToNextPhase()
            phase_fraction = 0.0

        if self.isFinished():
            # Pattern is finished. Nothing to do.
            return

        theta = phase_fraction * np.pi


        # This should work for all phases.
        theta += np.pi
        self.pattern_param['size'] = (
            (np.cos(theta)+1.0)/2.0
            * (self.pattern_param['phase_end_value'] - self.pattern_param['phase_start_value'])
            + self.pattern_param['phase_start_value'])


    def getLedScale(self):

        # Calculate with LED's should be on, and how much.
        self.calculateSize()

        scale = np.zeros(common.kNumLEDs)

        mask = self.index > self.pattern_options['location']
        mask &= self.index < (self.pattern_options['location']+self.pattern_param['size'])

        scale[mask] = 1.0

        if self.pattern_options['edge_fadeout']:
            edge_location = self.pattern_options['location']+self.pattern_param['size']-self.pattern_options['edge_size']
            edge_mask = (self.index >= edge_location)
            edge_mask &= mask

            edge = self.edgeFunction(self.index[edge_mask])

            scale[edge_mask] = edge

        return scale


    def edgeFunction(self, x, reverse_direction=False):
        """
        This returns an edge function specifically for this pattern.
        """
        if len(x) == 0:
            value = np.array([], dtype=common.k_default_dtype)
        elif len(x) == 1:
            value = np.array([0.5], dtype=common.k_default_dtype)
        else:
            theta = (x-x[0])/(x[-1]-x[0])*np.pi
            if reverse_direction:
                theta += np.pi
            value = (np.cos(theta)+1.0)/2.0

        return(value)


    def setLEDPatternParam(self):

        self.pattern_state['scale'] = self.getLedScale()


class PatternStaticSizePulser(PatternExpandAndContract):

    def defaultPatternOptions(self):
        options = super().defaultPatternOptions()

        options['location'] = (common.kNumLEDs-1)/2.0
        options['size_max_range'] = [(common.kNumLEDs-1)/4.0, (common.kNumLEDs-1)/2.0]
        options['size_min_range'] = [0.0, (common.kNumLEDs-1)/4.0]

        return options


    def getLedScale(self):

        # Calculate with LED's should be on, and how much.
        self.calculateSize()

        scale = np.zeros(common.kNumLEDs)

        mask = self.index >= self.pattern_options['location'] - self.pattern_param['size']
        mask &= self.index <= (self.pattern_options['location'] + self.pattern_param['size'])

        scale[mask] = 1.0

        if self.pattern_options['edge_fadeout']:
            edge_location_top = self.pattern_options['location']+self.pattern_param['size']-self.pattern_options['edge_size']
            edge_mask = (self.index >= edge_location_top) & (self.index > self.pattern_options['location'])
            edge_mask &= mask
            edge = self.edgeFunction(self.index[edge_mask])
            scale[edge_mask] = edge

            edge_location_bottom = self.pattern_options['location']-self.pattern_param['size']+self.pattern_options['edge_size']
            edge_mask = (self.index <= edge_location_bottom) & (self.index < self.pattern_options['location'])
            edge_mask &= mask
            edge = self.edgeFunction(self.index[edge_mask], reverse_direction=True)
            scale[edge_mask] = edge

        return scale