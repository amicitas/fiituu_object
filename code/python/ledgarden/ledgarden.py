#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  The LHD garden is a program to allow LEDs to be controled though an
  arduino.

"""

# Setup the basic logging configuration.
from mirutil import logging

import sys
import time
import copy
import threading

from collections import OrderedDict

import numpy as np

from . import common
from ._LEDGardenSerialArduino import LEDGardenSerialArduino
from .programs import *
from .modes import *

from . import serial_arduino

def getNewLedState(enabled=True
                   ,color=np.zeros(3), dtype=common.k_default_dtype):

    # Save the color as an float array for now.
    color = np.zeros((common.kNumLEDs, 3), dtype=common.k_default_dtype)
    mask = np.zeros(common.kNumLEDs, dtype=bool)
    mask[:] = enabled

    return {'enabled':mask
            ,'color':color}


def buttonState(state_current=False
                ,state_last=False):

    return {'state_current':state_current
            ,'state_last':state_last}



class LEDGarden:
    """
    Programming Notes:
      This class keeps track of two LED state arrays.

      led_state:
        The current LED state, or the one we are about to send.
      led_state_next:
        The next LED state that we are preparing.

      All of the programs will operate on led_state_next. At the start
      of a cycle led_state_next will be copied to led_state, then sent
      out to the arduino.  This separation allows me to simultaneously
      prepare a new state and send the last state.
    """


    def __init__(self, user_config=None):
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.info('Initializing the Heartpath Object.')

        self.setUserConfig(user_config)

        # Initialize the time stamps.  
        # These are needed elsewhere for initialization.
        common.mTimeStart = time.time()
        common.mRuntimeCycleCurrent = 0.0

        # Careful!  These are referenced all over the place do not reassign!
        self.led_state = getNewLedState()
        self.led_state_next = getNewLedState()

        self.command_queue = []
        self.command_queue.append(('OPTION_MIRROR_REVERSED', common.options['mirror_mode_reversed']))

        self.button_state = []
        self.initButtonState()

        # Define program lists:
        #  Background Programs: Always run as long as mode is active.
        #  Idle Programs:       Run when no action programs are active.
        #  Action Programs:     Run in reaction to some action (like a button press.
        #                       These should have a finite lifetime.
        #  Other Programs:      For special mode use. These will be cleaned
        #                       up when the mode is deselected, but otherwise
        #                       don't have any attached actions or effects on
        #                       any other programs.
        self.background_programs = []
        self.idle_programs = []
        self.action_programs = []
        self.other_programs = []

        self.modes = LEDGardenModesCollection(self)

        if common.options['arduino_usb_connection']:
            self.log.info('Connecting to the Arduino.')
            self.serialio = LEDGardenSerialArduino()
            self.serialio.connect()

        self.activateMode()
        #self.idle_programs.append(ProgramHeartbeatTwinkle(self.led_state_next))

        # Setup the default idle program.
        # if True:
        #     # I really like these two together, especially with the number of
        #     # hotaru turned up to 0.1 or something.
        #     self.idle_programs.append(ProgramSparklyKnightRider(self.led_state_next))
        # if False:
        #     self.hotaru1 = ProgramHotaru(self.led_state_next, np.array([0.5, 0.5, 0.5]), dtype=common.k_default_dtype)
        #     #self.hotaru2 = ProgramHotaru(self.led_state_next, np.array([0.5, 0.0, 1.0]), dtype=common.k_default_dtype)
        #     self.hotaru3 = ProgramHotaru(self.led_state_next, np.array([1.0, 0.0, 0.0]), dtype=common.k_default_dtype)
        #     self.hotaru1.config['end_after_lifetime'] = False
        #     #self.hotaru2.config['end_after_lifetime'] = False
        #     self.hotaru3.config['end_after_lifetime'] = False
        #     self.idle_programs.append(self.hotaru1)
        #     #self.idle_programs.append(self.hotaru2)
        #     self.idle_programs.append(self.hotaru3)
        # if False:
        #     self.heartbeat1 = ProgramHeartbeat(self.led_state_next)
        #     self.heartbeat2 = ProgramHeartbeat(self.led_state_next)
        #     self.heartbeat1.config['end_after_lifetime'] = False
        #     self.heartbeat2.config['end_after_lifetime'] = False
        #     self.idle_programs.append(self.heartbeat1)
        #     self.idle_programs.append(self.heartbeat2)

        self.log.debug('Init complete.')


    def setUserConfig(self, user_config=None):
        if user_config:
            for option in user_config:
                common.options[option] = user_config[option]


    def loop(self):
        while True:
            self.run()


    def run(self):
        """
        Run the next cycle.

        This will be called repeateded within the main loop.
        """


        common.mCycleCurrent += 1
        common.mRuntimeCycleLast = common.mRuntimeCycleCurrent
        common.mRuntimeCycleCurrent = time.time() - common.mTimeStart

        time_start = time.time()
        time_transfer = 0.0
        time_cycle = 0.0
        time_after_ready = 0.0
        time_after_buttons = 0.0

        if common.options['arduino_usb_connection']:
            self.waitForArduinoLoopReady()
        time_after_ready = time.time()

        if common.options['arduino_usb_connection']:
            self.waitForButtonUpdate()
        time_after_buttons = time.time()

        self.sendCommands()

        # Try to implement some multi threading.
        use_multi_threading = False
        if use_multi_threading:
            prep_thread = threading.Thread(target=self.prepareNextLedState)
            send_thread = threading.Thread(target=self.sendCurrentLedState)

            time_temp = time.time()
            prep_thread.start()
            send_thread.start()


            send_thread.join()
            time_transfer = time.time()-time_temp

            prep_thread.join()
            time_programs = time.time()-time_temp
        else:
            time_temp = time.time()
            self.prepareNextLedState()
            time_programs = time.time()-time_temp

            time_temp = time.time()
            self.sendCurrentLedState()
            time_transfer = time.time()-time_temp

        self.copyNextStateToCurrent()

        time_after_programs = time.time()

        time_cycle = time.time()-time_start
        time_ready = time_after_ready - time_start
        time_buttons = time_after_buttons - time_after_ready

        if common.options['debug_cycle']:
            self.log.debug('Python cycle time:  {} ms, Ready time: {} ms, Program time: {} ms, Data transfer time: {} ms'.format(
                int(time_cycle*1000.)
                ,int(time_ready*1000.)
                ,int(time_programs*1000.)
                ,int(time_transfer*1000.)))
            self.debugProgramsPatterns()



    def prepareNextLedState(self):
        """
        Run all of the LED programs.
        This will prepare the next LED state to be sent to the arduino.
        """
        self.clearLEDValues()

        self.runModes()
        self.runPrograms()

        self.applyColorShift()
        self.applyColorSwap()
        self.applyBrightnessCorrection()
        self.cleanupLEDValues()

        # This should be done last.
        self.applyColorCorrection()


    def sendCurrentLedState(self):
        """
        Send the current LED state to the Arduino.
        """

        if common.options['arduino_usb_connection']:
            # Send the LED state.
            self.sendLEDValues()

            # Tell the Arduino that we done and request a response.
            self.serialio.requestReady()


    def runModes(self):
        self.modes.run()


    def runPrograms(self):
        """
        This is the main loop that will be continuously called.
        """
        self.clearFinishedPrograms()
        self.updatePrograms()

        for program in self.background_programs:
            program.run()
        for program in self.idle_programs:
            program.run()
        for program in self.action_programs:
            program.run()
        for program in self.other_programs:
            program.run()


    def updatePrograms(self):
        if len(self.action_programs) == 0:
            for prog in self.idle_programs:
                if not prog.isActive():
                    prog.actionStartFadein()


    def sendCommands(self):
        """
        Send a command to the arduino.
        """
        while self.command_queue:
            command_tuple = self.command_queue.pop(0)

            if common.options['arduino_usb_connection']:
                self.serialio.sendCommand(command_tuple[0], command_tuple[1])


    def waitForArduinoLoopNext(self):
        timeout = 0.5
        time_last_request = time.time()

        #serial_arduino.byteio.flush()
        while True:
            response = self.serialio.getNextResponse()

            if response:
                if response[0] == 'NEXT':
                    return
            else:
                if (time.time() - time_last_request) > timeout:
                    raise Exception('Timeout.')
                #else:
                #    time.sleep(common.k_serial_refresh_time)


    def waitForArduinoLoopReady(self):
        timeout = 0.5

        self.serialio.requestReady()
        time_last_request = time.time()

        #serial_arduino.byteio.flush()
        while True:
            response = self.serialio.getNextResponse()

            if response:
                if response[0] == 'BUTTON':
                    self.updateButtonState(response)
                if response[0] == 'OK':
                    return
            else:
                if (time.time() - time_last_request) > timeout:
                    self.serialio.requestReady()
                    time_last_request = time.time()
                #else:
                #    time.sleep(common.k_serial_refresh_time)


    def waitForButtonUpdate(self):
        button_state = self.serialio.waitForArduinoButtonUpdate()

        #for ii, button in enumerate(button_state):
        #    print('PYTHON: Button {}: {}'.format(ii, button_state))

        self.updateButtonState(button_state)



    def clearFinishedPrograms(self):
        """
        Check to see if any of the programs are finished.  
        If so remove them from the program list.
        """

        for ii, program in enumerate(self.background_programs):
            if program.isFinished():
                self.log.debug('Removing Background Program: {}'.format(program.__class__.__name__))
                del self.background_programs[ii]

        for ii, program in enumerate(self.idle_programs):
            if program.isFinished():
                self.log.debug('Removing Idle Program: {}'.format(program.__class__.__name__))
                del self.idle_programs[ii]
                                     
        for ii, program in enumerate(self.action_programs):
            if program.isFinished():
                self.log.debug('Removing Action Program: {}'.format(program.__class__.__name__))
                del self.action_programs[ii]

        for ii, program in enumerate(self.other_programs):
            if program.isFinished():
                self.log.debug('Removing Other Program: {}'.format(program.__class__.__name__))
                del self.other_programs[ii]


    def initButtonState(self):
        default_state = [buttonState() for ii in range(common.options['num_buttons'])]

        self.button_state.extend(default_state)


    def updateButtonState(self, new_state):
        if len(new_state) != len(self.button_state):
            raise Exception('Length of recieved button state does not match expeceted length.')

        for ii, state in enumerate(self.button_state):
            state['state_last'] = state['state_current']
            state['state_current'] = new_state[ii]


    def deactivateActionPrograms(self, fadeout_time=2.0):
        for prog in self.action_programs:
            prog.endAfterFadeout(fadeout_time=fadeout_time)


    def deactivateIdlePrograms(self, fadeout_time=2.0):
        for prog in self.idle_programs:
            prog.endAfterFadeout(fadeout_time=fadeout_time)


    def deactivateOtherPrograms(self, fadeout_time=2.0):
        for prog in self.other_programs:
            prog.endAfterFadeout(fadeout_time=fadeout_time)


    def deactivatePrograms(self, fadeout_time=2.0):
        self.log.info('Deactivating Programs.')
        for prog in self.background_programs:
            prog.endAfterFadeout(fadeout_time=fadeout_time)
        for prog in self.idle_programs:
            prog.endAfterFadeout(fadeout_time=fadeout_time)
        for prog in self.action_programs:
            prog.endAfterFadeout(fadeout_time=fadeout_time)
        for prog in self.other_programs:
            prog.endAfterFadeout(fadeout_time=fadeout_time)


    def cycleToNextMode(self, reverse=False):
        mode = self.modes.getNextMode(reverse=reverse)
        self.activateMode(mode)


    def activateMode(self, name=None):
        self.log.info('Activating new mode: {}'.format(self.modes.current_mode))
        self.deactivatePrograms()

        self.modes.selectMode(name=name)
        self.modes.activate()

        background_programs = self.modes.getBackgroundPrograms()
        for item in background_programs:
            program = item[0](self.led_state_next, options=item[1])
            self.background_programs.append(program)
            program.actionStartFadein()

        idle_programs = self.modes.getIdlePrograms()
        for item in idle_programs:
            program = item[0](self.led_state_next, options=item[1])
            self.idle_programs.append(program)
            program.actionStartFadein()


    def startOtherProgram(self, program_def, fadein=True):

        new_program = program_def[0](self.led_state_next, options=program_def[1])
        self.other_programs.append(new_program)
        if fadein:
            new_program.actionStartFadein()


    def startIdleProgram(self, program_def):

        new_program = program_def[0](self.led_state_next, options=program_def[1])
        self.idle_programs.append(new_program)
        if len(self.action_programs) == 0:
            new_program.actionStartFadein()


    def startActionProgram(self, program_def):
        
        if len(self.action_programs) > common.k_max_running_programs:
            self.log.warning('Cannot activate new program. Too many programs running.')
            return

        # If there are currently no active Action Programs, then this call adds the first.
        # Therefore start the immediate fadeout of the idle_programs.
        if len(self.action_programs) == 0:
            for program in self.idle_programs:
                program.actionStartFadeout()

        new_program = program_def[0](self.led_state_next, options=program_def[1])
        self.action_programs.append(new_program)
        new_program.actionStartFadein()

        max_lifetime = 0.0
        for program in self.action_programs:
            max_lifetime = max(max_lifetime, program.timeToLifetime())

        for program in self.idle_programs:
            program.time_fadein_start = common.runtimeCycle() + max_lifetime


    def clearLEDValues(self):
        self.led_state_next['color'][:] = 0


    def sendLEDValues(self):
        #for ii, state in enumerate(self.led_state):
        #    print('PYTHON STATE ({}): {}'.format(ii, state['value']))
        self.serialio.writeLEDValues(self.led_state)

        
    def cleanupLEDValues(self, state=None):
        if state is None:
            state = self.led_state_next

        mask = state['color'] < common.kLEDMinValue
        state['color'][mask] = common.kLEDMinValue

        mask = state['color'] > common.kLEDMaxValue
        state['color'][mask] = common.kLEDMaxValue


    def copyNextStateToCurrent(self):
        """
        Copy led_state_next to led_state.
        """
        for key in self.led_state:
            if isinstance(self.led_state[key], np.ndarray):
                self.led_state[key][:] = self.led_state_next[key][:]
            else:
                self.led_state[key] = copy.deepcopy(self.led_state_next[key])


    def applyBrightnessCorrection(self, state=None):
        """
        Do a global brightness correction.
        """
        if state is None:
            state = self.led_state_next

        state['color'][:,:] *= common.options['brightness']


    def applyColorCorrection(self, state=None):
        """
        Do a color correction to make the LED color match my screen color.
        """
        if state is None:
            state = self.led_state_next

        # Check the correction value.
        if max(common.options['color_correction']) > 1.0:
            self.log.warning('Color correction out of range.')

        for ii in range(common.kLEDNumColors):
            state['color'][:,ii] *= common.options['color_correction'][ii]


    def applyColorShift(self, state=None):
        """
        Do a global color shift.
        """
        if state is None:
            state = self.led_state_next

        if common.options['color_shift'] == 0:
            return

        fraction = common.options['color_shift']%1.0
        shift = int(np.floor(common.options['color_shift']))
        index1 = np.roll(np.array([0,1,2]), shift)
        index2 = np.roll(np.array([0,1,2]), shift+1)

        norm_old = np.linalg.norm(state['color'], axis=1)
        # The normalization can break down for very small numbers that fall outside
        # the current floating point accuracy.  Also we don't want to devide by zero.
        mask = norm_old > 1e-6
        color_new = (state['color'][:,index1]*(1.0 - fraction) + state['color'][:,index2]*fraction)
        norm_new = np.linalg.norm(color_new, axis=1)
        norm_factor = norm_old[mask]/norm_new[mask]
        state['color'][mask,:] = color_new[mask, :] * norm_factor[:, np.newaxis]


    def applyColorSwap(self, state=None):
        """
        Do a global color shift.
        """
        if state is None:
            state = self.led_state_next

        if common.options['color_swap'] == 0:
            return

        old_color = state['color'].copy()

        if common.options['color_swap'] > 1.0:
            fraction = 2.0 - common.options['color_swap']
        else:
            fraction = common.options['color_swap']

        # This is the simplest color swap.
        #state['color'][:, 0] = (old_color[:,1]+old_color[:,2])/2.0*fraction + old_color[:,0]*(1-fraction)
        #state['color'][:, 1] = (old_color[:,0]+old_color[:,2])/2.0*fraction + old_color[:,1]*(1-fraction)
        #state['color'][:, 2] = (old_color[:,0]+old_color[:,1])/2.0*fraction + old_color[:,2]*(1-fraction)

        # Trying something a little more complicated.
        if fraction < 0.5:
            frac_1 = fraction*2
            frac_2 = 0.0
        else:
            frac_1 = 1.0
            frac_2 = (fraction - 0.5)*2

        state['color'][:, 0] = old_color[:,2]*frac_1+old_color[:,1]*frac_2 + old_color[:,0]*(1-fraction)
        state['color'][:, 1] = old_color[:,0]*frac_1+old_color[:,2]*frac_2 + old_color[:,1]*(1-fraction)
        state['color'][:, 2] = old_color[:,1]*frac_1+old_color[:,0]*frac_2 + old_color[:,2]*(1-fraction)


        norm_old = np.linalg.norm(old_color, axis=1)
        # The normalization can break down for very small numbers that fall outside
        # the current floating point accuracy.  Also we don't want to devide by zero.
        mask = norm_old > 1e-6
        norm_new = np.linalg.norm(state['color'], axis=1)
        norm_factor = norm_old[mask]/norm_new[mask]
        state['color'][mask, :] = state['color'][mask, :] * norm_factor[:, np.newaxis]

        #print(old_color[0,:], state['color'][0,:])


    def actionButtonPress(self, ii):
        """
        Handle a button press event.
        Note:  Button presses captured from hardware will be processes directly in the
               mode dispatcher object.
        """
        self.modes.actionButtonPress(ii)


    def actionButtonToggle(self, ii):
        """
        Handle a button toggle event.

        The idea here is simulate a button being held down for testing.

        This method is only used by the simulator.
        When the arduino is hooked up the button state is entirely controled
        by the arduino and this method will have no effect.
        """
        if common.options['arduino_usb_connection']:
            self.log.warning('actionButtonToggle has no effect when the Arduino is connected.')
            return

        # Simulate a button being held down.
        # So both current and previous state should be the same.
        self.button_state[ii]['state_current'] = not self.button_state[ii]['state_current']
        self.button_state[ii]['state_last'] = self.button_state[ii]['state_current']
        self.log.debug('Set button {} state to {}'.format(ii, self.button_state[ii]['state_current']))


    def actionCrazyMode(self):
        self.modes.actionCrazyMode()


    def resetOptions(self):
        """
        Reset all the user modified options to their defaults.
        """
        self.resetSpeed()
        self.resetBrightness()
        self.resetColorShift()
        self.resetColorSwap()

        self.command_queue.append(('OPTION_MIRROR_REVERSED', True))



    def increaseSpeed(self):
        """
        Increase the global program speed by 10%.
        """
        common.options['speed'] += 1
        self.log.info('Speed set to: {:0.2f}'.format(common.options['speed']))


    def decreaseSpeed(self):
        """
        Decrease the global program speed by 10%.
        """
        common.options['speed'] -= 1
        self.log.info('Speed set to: {:0.2f}'.format(common.options['speed']))


    def resetSpeed(self):
        common.options['speed'] = 0


    def increaseBrightness(self):
        """
        Increase the global brightness.
        """
        if common.options['brightness'] <= 0.9:
            common.options['brightness'] += 0.1
        else:
            common.options['brightness'] = 1.0

        self.log.info('Brightness set to: {:0.2f}'.format(common.options['brightness']))


    def decreaseBrightness(self):
        """
        Decrease the global brightness.
        """
        if common.options['brightness'] >= 0.1:
            common.options['brightness'] -= 0.1
        else:
            common.options['brightness'] = 0.0

        self.log.info('Brightness set to: {:0.2f}'.format(common.options['brightness']))


    def resetBrightness(self):
        common.options['brightness'] = 1.0


    def increaseColorShift(self):
        """
        Increase the global colorshift.
        """

        common.options['color_shift'] += 0.1

        if common.options['color_shift'] > 3.0:
            common.options['color_shift'] -= 6.0

        self.log.info('colorshift set to: {:0.2f}'.format(common.options['color_shift']))


    def decreaseColorShift(self):
        """
        Decrease the global colorshift.
        """

        common.options['color_shift'] -= 0.1
        if common.options['color_shift'] < -3.0:
            common.options['color_shift'] += 6.0

        self.log.info('colorshift set to: {:0.2f}'.format(common.options['color_shift']))


    def resetColorShift(self):
        common.options['color_shift'] = 0.0


    def increaseColorSwap(self):
        """
        Increase the global colorswap.
        """

        common.options['color_swap'] += 0.05

        if common.options['color_swap'] > 2.0:
            common.options['color_swap'] =  0.0

        self.log.info('colorswap set to: {:0.2f}'.format(common.options['color_swap']))


    def decreaseColorSwap(self):
        """
        Decrease the global colorswap.
        """

        common.options['color_swap'] -= 0.05
        if common.options['color_swap'] < 0.0:
            common.options['color_swap'] = 2.0

        self.log.info('colorswap set to: {:0.2f}'.format(common.options['color_swap']))


    def resetColorSwap(self):
        common.options['color_swap'] = 0.0


    def debugProgramsPatterns(self):

        num_programs = 0
        num_patterns = 0
        program_lists = [self.background_programs, self.idle_programs, self.action_programs]
        for prog_list in program_lists:
            num_programs += len(prog_list)
            for prog in prog_list:
                num_patterns += len(prog.patterns)

        self.log.debug('Num Programs: {}, Num Patterns: {}'.format(num_programs, num_patterns))


    def printDebugInfo(self):

        self.log.debug('Background programs:')
        self.log.debug(self.background_programs)

        self.log.debug('Idle programs:')
        self.log.debug(self.idle_programs)
        for prog in self.idle_programs:
            self.log.debug('{}, current_state: {}'.format(prog.__class__.__name__, prog.current_state))
            self.log.debug('runtimeCycle: {}, prog.time_fadein_start: {}'.format(common.runtimeCycle(), prog.time_fadein_start))

        self.log.debug('Action programs:')
        self.log.debug(self.action_programs)
        for prog in self.action_programs:
            for patt in prog.patterns:
                self.log.debug('{}, {} '.format(patt.__class__.__name__, patt.isFinished()))
                if not patt.isFinished():
                    self.log.debug('Options:')
                    self.log.debug(patt.pattern_options)
                    self.log.debug('Param:')
                    self.log.debug(patt.pattern_param)

        self.log.debug('Other programs:')
        self.log.debug(self.other_programs)
        for prog in self.other_programs:
            for patt in prog.patterns:
                self.log.debug('{}, {} '.format(patt.__class__.__name__, patt.isFinished()))
                if not patt.isFinished():
                    self.log.debug('Options:')
                    self.log.debug(patt.pattern_options)
                    self.log.debug('Param:')
                    self.log.debug(patt.pattern_param)


    def toggleMirrorMode(self):
        common.options['mirror_mode_reversed'] = not common.options['mirror_mode_reversed']
        self.command_queue.append(('OPTION_MIRROR_REVERSED', common.options['mirror_mode_reversed']))
