#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  Define the programs used for the LED Garden.

"""

# Setup the basic logging configuration.
from mirutil import logging

import time
import random
import copy

from . import common
from . import color
from .patterns import *


kInactive = 0
kActive = 1
kFadein = 2
kFadeout = 3
kFinished = 4

kSecondsWeek = 604800
kSecondsYear = 31557600

class LEDGardenProgram:
    """
    An LEDGardenProgram provides a way to package an run a set of LEDGardenPatterns.

    Note: When creating programs, the order in which the patterns are loaded
          is important for the final outcome.
    """

    def __init__(self, led_state, options=None):
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.debug('Launching program: {}'.format(self.__class__.__name__))

        self.led_state = led_state
        self.led_state_internal = copy.deepcopy(led_state)

        self.current_state = kInactive
        self.current_cycle = 0

        self.time_init = common.runtimeCycle()
        self.time_fadeout_start = self.time_init + kSecondsYear
        self.time_fadein_start = self.time_init + kSecondsYear

        self.patterns = []

        self.options = self.defaultProgramOptions()
        if options is not None:
            self.setUserOptions(options)

        self.param = self.defaultProgramParam()


    def defaultProgramOptions(self):
        return {'end_after_lifetime':False
                ,'time_fadeout':2.0
                ,'time_fadein':2.0
                ,'time_lifetime':5.0}


    def defaultProgramParam(self):
        return {}


    def setUserOptions(self, options=None):
        if options:
            self.options.update(options)


    def run(self):
        scale = 1.0

        self.checkNumPatterns()
        self.clearLEDValues()

        runtime = common.runtimeCycle()
        
        if self.options['end_after_lifetime']:
            if self.current_state == kFinished:
                return

            elif (runtime - self.time_init) > self.options['time_lifetime']:
                self.current_state = kFinished
                return

            elif (runtime - self.time_init) > (self.options['time_lifetime'] - self.options['time_fadeout']):
                if self.current_state != kFadeout:
                    self.actionStartFadeout()


        if self.current_state == kFadein:
            scale =  (runtime - self.time_fadein_start)/self.options['time_fadein']
            if (scale > 1.0):
                self.log.debug('{}: Fade in complete.'.format(self.__class__.__name__))
                self.actionFadeinComplete()
                scale = 1.0;

        elif self.current_state == kFadeout:
            scale = 1.0 - (runtime - self.time_fadeout_start)/self.options['time_fadeout']
            if (scale < 0.0):
                self.log.debug('{}: Fade out complete'.format(self.__class__.__name__))
                self.actionFadeoutComplete()
                scale = 0.0

        elif self.current_state == kInactive:
            scale = 0.0;
            if (runtime >= self.time_fadein_start):
                self.actionStartFadein()

        elif self.current_state == kActive:
            scale = 1.0
            if (runtime >= self.time_fadeout_start):
                self.actionStartFadeout()

        # Run all of the patterns.
        # Pass the scale, since we want the individual patterns to
        # scale themselves appropriately before adding themselves to
        # the LED setting.
        if not self.isInactive():
            for pat in self.patterns:
                pat.run()


        self.scaleLEDValue(scale)


        # Check that the internal LED state is within bounds.
        self.cleanupLEDValue()

        # Finally add the internal LED state to the global LED state.
        if 'enabled' in self.options:
            mask = self.options['enabled']
            self.led_state['color'][mask,:] += self.led_state_internal['color'][mask,:]
        else:
            self.led_state['color'] += self.led_state_internal['color']

        self.current_cycle += 1

        self.clearFinishedPatterns()


    def isActive(self):
        return (self.current_state == kActive
                or self.current_state == kFadein
                or self.current_state == kFadeout)


    def isInactive(self):
        return (self.current_state == kInactive)


    def isFinished(self):
        return (self.current_state == kFinished)


    def runtime(self):
        """
        Return the runtime for this particular program.
        """
        return common.runtimeCycle() - self.time_init


    def timeToFadeout(self):
        pass


    def timeToFadein(self):
        pass


    def timeToLifetime(self):
        return self.time_init + self.options['time_lifetime'] - common.runtimeCycle()


    def actionFadeinComplete(self):
        self.current_state = kActive
        self.time_fadein_start = self.time_init + kSecondsYear


    def actionFadeoutComplete(self):
        self.current_state = kInactive
        self.time_fadeout_start = self.time_init + kSecondsYear


    def actionStartFadein(self):
        if self.current_state != kFadein and self.current_state != kActive:
            self.log.debug('Starting fadein.')
            self.time_fadein_start = common.runtimeCycle()

            self.current_state = kFadein

    def actionStartFadeout(self):
        if self.current_state != kFadeout and self.current_state != kInactive:
            self.log.debug('Starting fadeout.')
            self.time_fadeout_start = common.runtimeCycle()

            self.current_state = kFadeout


    def endAfterFadeout(self, fadeout_time=None):
        """
        Tell the program to immediately fadeout and then end.
        """
        if fadeout_time is not None:
            self.options['time_fadeout'] = fadeout_time

        self.options['end_after_lifetime'] = True
        self.options['time_lifetime'] = (common.runtimeCycle() - self.time_init) + self.options['time_fadeout']


    def clearLEDValues(self):
        self.led_state_internal['color'][:] = 0


    def cleanupLEDValue(self):
        mask = self.led_state_internal['color'] < common.kLEDMinValue
        self.led_state_internal['color'][mask] = common.kLEDMinValue

        mask = self.led_state_internal['color'] > common.kLEDMaxValue
        self.led_state_internal['color'][mask] = common.kLEDMaxValue


    def scaleLEDValue(self, scale):
        """
        Scale all of the internal led colors.
        """
        for ii in range(common.kLEDNumColors):
            self.led_state_internal['color'][:,ii] *= scale


    def clearFinishedPatterns(self):
        """
        Check to see if any of the patterns are finished.
        If so remove them from the pattern list.
        """

        for ii, pattern in enumerate(self.patterns):
            if pattern.isFinished():
                del self.patterns[ii]


    def checkNumPatterns(self):
        if len(self.patterns) > common.k_max_program_patterns:
            self.log.warning("More than the maximum number of allowed program patterns has been created!")


    def appendPattern(self, pattern_def):

        if len(self.patterns) < common.k_max_program_patterns:
            pattern = pattern_def[0](self.led_state_internal, options=pattern_def[1])
            self.patterns.append(pattern)
        else:
            self.log.warning('Cannot activate new pattern. Too many patterns already running.')


    def insertPattern(self, pattern_def):

        if len(self.patterns) < common.k_max_program_patterns:
            pattern = pattern_def[0](self.led_state_internal, options=pattern_def[1])
            self.patterns.insert(0, pattern)
        else:
            self.log.warning('Cannot activate new pattern. Too many patterns already running.')


class ProgramTest000(LEDGardenProgram):

    def __init__(self, led_state):
        super().__init__(led_state)

        #self.test00 = PatternStrobeOn(self.led_state_internal)
        #self.test00 = PatternBubble(self.led_state_internal)
        #self.test00 = PatternColor(self.led_state_internal)
        self.test00 = PatternGradientScroll(self.led_state_internal)
        self.patterns.append(self.test00)


        #self.test01 = PatternTwinkleOff(self.led_state_internal)
        #self.patterns.append(self.test01)


class ProgramSparklyKnightRider(LEDGardenProgram):
    """
    A few traveling red spots, plus twinkling hotaru.

    This can be made much more relaxing by removing (or reducing)
    the twinkle, and reducing the fraction of active hotaru.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        options = {'color':np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
                   ,'location_start':0
                   ,'speed':20.0}
        self.test_00 = PatternSpotScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test_00)

        options = {'color':np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
                   ,'location_start':0
                   ,'speed':15.0}
        self.test_01 = PatternSpotScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test_01)

        options = {'color':np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
                   ,'location_start':0
                   ,'speed':-18.0}
        self.test_02 = PatternSpotScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test_02)

        self.twinkleoff = PatternTwinkleOff(self.led_state_internal)
        self.patterns.append(self.twinkleoff)

        options = {'color':np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
                   ,'fraction_active':0.1}
        self.hotaru = PatternHotaruColor(self.led_state_internal, options=options)
        self.patterns.append(self.hotaru)

        options = {}
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        self.twinkle = PatternTwinkleColor(self.led_state_internal, options=options)
        self.patterns.append(self.twinkle)

        #options = {'scale':0.1}
        #self.scale = PatternScale(self.led_state_internal, options=options)
        #self.patterns.append(self.scale)


class ProgramTripleKnightRider(LEDGardenProgram):
    """
    A few traveling red spots.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        options = {'color':np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
                   ,'location_start':0
                   ,'speed':20.0}
        self.test_00 = PatternSpotScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test_00)

        options = {'color':np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
                   ,'location_start':0
                   ,'speed':15.0}
        self.test_01 = PatternSpotScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test_01)

        options = {'color':np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
                   ,'location_start':0
                   ,'speed':-18.0}
        self.test_02 = PatternSpotScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test_02)

        self.twinkleoff = PatternTwinkleOff(self.led_state_internal)
        self.patterns.append(self.twinkleoff)


class ProgramSparklyHotaru(LEDGardenProgram):
    """
    Just some sparkly hotaru.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        options = {'color':np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
                   ,'fraction_active':0.1}
        self.hotaru = PatternHotaruColor(self.led_state_internal, options=options)
        self.patterns.append(self.hotaru)

        options = {}
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        self.twinkle = PatternTwinkleColor(self.led_state_internal, options=options)
        self.patterns.append(self.twinkle)


class FiiTuuTest001(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.color = PatternColor(self.led_state_internal, np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype))
        options_pulse = {'fraction_active':0.3}
        self.pulse = PatternPulse(self.led_state_internal, options=options_pulse)
        #self.twinkle = PatternTwinkleColor(self.led_state_internal, np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
        self.heartbeat = PatternHeartbeat(self.led_state_internal)

        self.patterns.append(self.color)
        self.patterns.append(self.pulse)
        #self.patterns.append(self.twinkle)
        self.patterns.append(self.heartbeat)


class FiiTuuTest002(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.color = PatternColor(self.led_state_internal, np.array([0.57, 0.0, 1.0], dtype=common.k_default_dtype))
        self.pulse = PatternPulse(self.led_state_internal)
        self.heartbeat = PatternHeartbeat(self.led_state_internal)
        self.twinkleoff = PatternTwinkleOff(self.led_state_internal)


        self.patterns.append(self.color)
        self.patterns.append(self.pulse)
        self.patterns.append(self.heartbeat)
        self.patterns.append(self.twinkleoff)


class FiiTuuTestHotaru(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.color = PatternColor(self.led_state_internal, np.array([0.57, 0.0, 1.0], dtype=common.k_default_dtype))
        self.hotaru = PatternHotaru(self.led_state_internal)

        self.patterns.append(self.color)
        self.patterns.append(self.hotaru)


class FiiTuuTestHotaruColor(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.hotaru = PatternHotaruColor(self.led_state_internal, np.array([0.8, 1.0, 0.0], dtype=common.k_default_dtype))

        self.patterns.append(self.hotaru)


class ProgramColor(LEDGardenProgram):


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        options = {}
        options['color'] = self.options['color']
        self.patt = PatternColor(self.led_state_internal, options=options)
        self.patterns.append(self.patt)



    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['color'] = [0.0, 1.0, 0.0]

        return options


class ProgramHeartbeatTwinkle(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.color = PatternColor(self.led_state_internal, np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype))
        self.pulse = PatternPulse(self.led_state_internal)
        self.heartbeat = PatternHeartbeat(self.led_state_internal)
        self.twinkle = PatternTwinkleOff(self.led_state_internal)

        #self.patterns.append(PatternOn(self.led_state))
        self.patterns.append(self.color)
        self.patterns.append(self.pulse)
        self.patterns.append(self.heartbeat)
        #self.patterns.append(self.twinkle)

        self.heartbeat.disableLEDs()
        self.heartbeat.enableLEDs()


class ProgramHotaru(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        options = {}
        options['color'] = self.options['color']
        self.patt00 = PatternHotaruColor(self.led_state_internal, options)

        self.patterns.append(self.patt00)


    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['color'] = np.array([0.8, 1.0, 0.0], dtype=common.k_default_dtype)
        return options


class ProgramHeartbeat(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.heartbeat = PatternHeartbeatColor(self.led_state_internal)
        self.patterns.append(self.heartbeat)


class ProgramFranticTwinkle(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        options = {}
        options['fraction_active'] = 0.7
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        options['min_effect'] = 0.0
        options['max_effect'] = 1.0
        options['min_period'] = 0.2
        options['max_period'] = 0.5
        self.test02 = PatternTwinkleColor(self.led_state_internal, options=options)
        #self.test02.time_start = common.runtime()#+1.5
        self.patterns.append(self.test02)


class ProgramStrobeLight(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.test00 = PatternStrobeOn(self.led_state_internal, options=self.options)

        self.patterns.append(self.test00)


class ProgramStrobeTwinkle(LEDGardenProgram):
    # Currently Broken.

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        self.on = PatternColor(self.led_state_internal, np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype))
        self.twinkle = PatternTwinkleOff(self.led_state_internal)

        self.patterns.append(self.on)
        self.patterns.append(self.twinkle)

        self.twinkle.min_effect = 0.0
        self.twinkle.max_effect = 2.0
        self.min_period = 0.2
        self.max_period = 1.5
        self.period_repeat = 2

        self.options['end_after_lifetime'] = True
        self.options['time_lifetime'] = common.kProgramLifetime


        sef.log.info('Initializing Strobe program.')


class ProgramTravelingSpotRandom(LEDGardenProgram):
    """
    Creates a traveling spot with a random speed.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        speed = np.random.uniform(5.0, 30.0)
        sign = np.random.random() > 0.5
        if sign:
            speed *= -1

        options = {'color':self.options['color']
                   ,'location_start':0
                   ,'speed':speed}
        self.test_00 = PatternSpotScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test_00)


    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['color'] = np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
        return options


class ProgramTravellingSpotBurst(LEDGardenProgram):
    """
    Creates a spot that travels outward from the center.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        speed = 10.0

        options = {'color':self.options['color']
                   ,'location_min':common.kNumLEDs/2
                   ,'location_max':common.kNumLEDs+10
                   ,'location_start':common.kNumLEDs/2
                   ,'speed':speed
                   ,'finish_at_bounds':True
                   ,'wrap_around':False}
        pattern = PatternSpotScroll(self.led_state_internal, options=options)
        self.patterns.append(pattern)


        options = {'color':self.options['color']
                   ,'location_min':-10
                   ,'location_max':common.kNumLEDs/2-1
                   ,'location_start':common.kNumLEDs/2-1
                   ,'speed':speed*-1
                   ,'finish_at_bounds':True
                   ,'wrap_around':False}
        pattern = PatternSpotScroll(self.led_state_internal, options=options)
        self.patterns.append(pattern)


    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['end_after_lifetime'] = False
        options['color'] = np.array([1.0, 0.0, 0.0], dtype=common.k_default_dtype)
        return options


    def isFinished(self):
        """
        For this program we are finished when all of burst patternshave ended.
        """
        # There is one twinkle pattern.
        return len(self.patterns) <= 1


class ProgramBubbleLauncher(LEDGardenProgram):
    """
    Randomly launches bubbles.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        options = {}
        options['max_period'] = 1.0
        options['fraction_active'] = 0.5

        twinkle = PatternTwinkleOff(self.led_state_internal, options=options)
        self.patterns.append(twinkle)


    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['color_scheme'] = 'blue-green'
        return options


    def defaultProgramParam(self):
        param = super().defaultProgramParam()
        param['time_last'] = 0.0
        param['time_next'] = 0.0
        return param


    def launchNewBubble(self):
        """Launch a new bubble pattern."""

        if self.options['color_scheme'] == 'blue-green':
            red = np.random.uniform(0.0, 0.25)
            green = np.random.uniform(0.0, 1.0)
            blue = np.random.uniform(0.5, 1.0)
        color = np.array([red, green, blue], dtype=common.k_default_dtype)

        width = np.random.uniform(0.5, 1.5)
        options = {'color':color
                   ,'width':width}

        pattern_def = (PatternBubble, options)
        self.insertPattern(pattern_def)


    def run(self):

        # Check to see if we need to launch a new bubble.
        if common.runtimeCycle() > self.param['time_next']:
            self.launchNewBubble()
            self.param['time_last'] = common.runtimeCycle()
            self.param['time_next'] = common.runtimeCycle() + np.random.uniform(0.1, 10.0)

        super().run()


class ProgramBubbleBurst(LEDGardenProgram):
    """
    Creates a burst of bubbles.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        twinkle = PatternTwinkleOff(self.led_state_internal)
        self.patterns.append(twinkle)

        num_bubbles = 10
        for ii in range(num_bubbles):
            options = {}
            options['width'] = np.random.uniform(0.5, 1.5)
            options['speed_range'] = [0.5, 30.0]

            if self.options['color_scheme'] == 'blue':
                red = np.random.uniform(0.0, 0.25)
                green = np.random.uniform(0.0, 0.5)
                blue = np.random.uniform(0.5, 1.0)

            elif self.options['color_scheme'] == 'green':
                red = np.random.uniform(0.0, 0.25)
                green = np.random.uniform(0.5, 1.0)
                blue = np.random.uniform(0.0, 0.5)

            elif self.options['color_scheme'] == 'red':
                red = np.random.uniform(0.5, 1.0)
                green = np.random.uniform(0.0, 0.25)
                blue = np.random.uniform(0.0, 0.5)

            color = np.array([red, green, blue], dtype=common.k_default_dtype)
            options['color'] = color

            pattern_def = (PatternBubble, options)
            self.insertPattern(pattern_def)



    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['end_after_lifetime'] = False
        options['color_scheme'] = 'blue'
        return options


    def run(self):
        """
        For this program we are finished when all of bubble patterns have ended.
        """

        if len(self.patterns) <= 1:
            self.current_state = kFinished

        super().run()


class ProgramRainbowScroll(LEDGardenProgram):
    """
    A scrolling rainbow gradient.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        options = {}
        options['gradient'] = self.options['gradient']
        self.test00 = PatternGradientScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test00)

    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['gradient'] = 'rainbow cycle'
        return options


class ProgramGradientBarPulser(LEDGardenProgram):
    """
    Pulse the height of the leds starting from zero.
    The colors will be a moving gradient.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        options = {}
        options['gradient'] = self.options['gradient']
        self.test00 = PatternGradientScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test00)

        options = {}
        #options['gradient'] = self.options['gradient']
        self.test01 = PatternExpandAndContract(self.led_state_internal, options=options)
        self.patterns.append(self.test01)

    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['gradient'] = 'rainbow cycle'
        return options


class ProgramFire(LEDGardenProgram):
    """
    Pulse the height of the leds starting from zero.
    The colors will be a moving gradient.

    This is basically similar to ProgramGradientBarPulser, but with the options
    set specifically to look more fire like.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.gradient_names = [
            'fire'
            ,'rainbow cycle'
            ,'rastaman'
            ,'miami sunset'
            ,'wavy davey']
        index = int(np.round(np.random.uniform(0.0, len(self.gradient_names)-1)))

        if self.options['random_gradient']:
            gradient = self.gradient_names[index]
        else:
            gradient = self.options['gradient']

        options = {}
        options['gradient'] = gradient
        patt = PatternGradientScroll(self.led_state_internal, options=options)
        self.patterns.append(patt)

        options = {}
        options['location'] = 0.0
        options['size_max_range'] = self.options['size_max_range']
        options['size_min_range'] = self.options['size_min_range']
        options['uptime_range'] = self.options['uptime_range']
        options['downtime_range'] = self.options['uptime_range']
        self.patt = PatternExpandAndContract(self.led_state_internal, options=options)
        self.patterns.append(self.patt)


    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['gradient'] = 'fire'
        options['random_gradient'] = False
        options['size_max_range'] = [common.kNumLEDs/4.0, common.kNumLEDs]
        options['size_min_range'] = [0.0, common.kNumLEDs*0.75]
        options['uptime_range'] = [0.5, 3.0]
        options['downtime_range'] = [0.5, 2.0]
        return options


class ProgramButtonControledFire(ProgramFire):

    def run(self):

        # Gradually reduce the pattern height.
        time = common.runtimeCycle() - common.mRuntimeCycleLast
        increment = 0.020/time/1.50

        for patt in self.patterns:
            if isinstance(patt, PatternExpandAndContract):
                patt.pattern_options['size_max_range'][0] -= increment
                patt.pattern_options['size_max_range'][1] -= increment*0.8
                patt.pattern_options['size_min_range'][0] -= increment
                patt.pattern_options['size_min_range'][1] -= increment*0.6

                # Limit the minimum range.
                if patt.pattern_options['size_max_range'][1] < 0:
                    patt.pattern_options['size_max_range'][0] = 0.0
                    patt.pattern_options['size_max_range'][1] = 0.0
                    patt.pattern_options['size_min_range'][0] = 0.0
                    patt.pattern_options['size_min_range'][1] = 0.0

        # Now run all the patterns.
        super().run()

    def increaseHeight(self):
        time = common.runtimeCycle() - common.mRuntimeCycleLast
        increment = 0.020/time/0.50

        for patt in self.patterns:
            if isinstance(patt, PatternExpandAndContract):
                patt.pattern_options['size_max_range'][0] += increment
                patt.pattern_options['size_max_range'][1] += increment*0.8
                patt.pattern_options['size_min_range'][0] += increment
                patt.pattern_options['size_min_range'][1] += increment*0.6

                # Limit the maxiumum range.
                if patt.pattern_options['size_max_range'][1] > common.kNumLEDs:
                    patt.pattern_options['size_max_range'][0] = common.kNumLEDs
                    patt.pattern_options['size_max_range'][1] = common.kNumLEDs*0.9
                    patt.pattern_options['size_min_range'][0] = common.kNumLEDs
                    patt.pattern_options['size_min_range'][1] = common.kNumLEDs*0.8



class ProgramCentralPulser(LEDGardenProgram):
    """
    Pulse the height of the leds starting from zero.
    The colors will be a moving gradient.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        options = {}
        options['gradient'] = self.options['gradient']
        self.test00 = PatternGradientScroll(self.led_state_internal, options=options)
        self.patterns.append(self.test00)

        # For now start in the middle.
        options = {}
        #options['gradient'] = self.options['gradient']
        self.test01 = PatternStaticSizePulser(self.led_state_internal, options=options)
        self.patterns.append(self.test01)

    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['gradient'] = 'rainbow cycle'
        return options


class ProgramHanami(LEDGardenProgram):
    """
    Launch a firework!
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


        self.gradient_names = ['rainbow cycle', 'rastaman', 'miami sunset']
        index = int(np.round(np.random.uniform(0.0, len(self.gradient_names)-1)))

        if self.options['random_gradient']:
            gradient = self.gradient_names[index]
        else:
            gradient = self.options['gradient']

        # The order of these patterns is important.
        if self.options['sparkler']:

            value_list = np.random.uniform(0.0, 1.0, 3)
            for value in value_list:
                options = {}
                options['fraction_active'] = 0.4
                options['color'] = color.gradient(value, gradient)
                options['min_effect'] = 0.0
                options['max_effect'] = 1.0
                options['min_period'] = 0.1
                options['max_period'] = 1.0
                options['time_start'] = 0.0
                options['time_fadein'] = 0.0
                patt = PatternTwinkleColor(self.led_state_internal, options=options)
                self.patterns.append(patt)

        elif not self.options['use_gradient']:
            options = {}
            value = np.random.uniform(0.0, 1.0)
            options['color'] = color.gradient(value, gradient)
            patt = PatternColor(self.led_state_internal, options=options)
            self.patterns.append(patt)
        else:
            options = {}
            options['gradient'] = gradient
            options['speed'] = 60
            patt = PatternGradientScroll(self.led_state_internal, options=options)
            self.patterns.append(patt)

        options = {}
        options['fraction_active'] = 0.7
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        options['min_effect'] = 0.0
        options['max_effect'] = 1.0
        options['min_period'] = 0.1
        options['max_period'] = 0.5
        options['time_start'] = common.runtime()+1.5
        options['time_fadein'] = 0.5
        self.test02 = PatternTwinkleColor(self.led_state_internal, options=options)
        self.patterns.append(self.test02)

        location_range = [common.kNumLEDs/8.0, common.kNumLEDs - common.kNumLEDs/8.0]
        options = {}
        options['location'] = np.random.uniform(location_range[0], location_range[1])
        options['size_max_range'] = [10, common.kNumLEDs/2.0]
        options['size_min_range'] = [0.0,0.0]
        options['uptime_range'] = [1.0, 3.0]
        options['downtime_range'] = [5.0, 10.0]
        self.test01 = PatternStaticSizePulser(self.led_state_internal, options=options)
        self.patterns.append(self.test01)


    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['gradient'] = 'rainbow cycle'
        options['random_gradient'] = False
        options['sparkler'] = False
        options['use_gradient'] = False

        options['end_after_lifetime'] = True
        options['time_lifetime'] = 4.0
        options['time_fadeout'] = 1.0
        options['time_fadein'] = 0.1

        return options


class ProgramCrazyProgram(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        index = np.linspace(0.0, common.kNumLEDs-1, common.kNumLEDs)

        mask = index > (common.kNumLEDs-1)/2.0
        mask = index%2 == 0

        options = {}
        options['gradient'] = 'blue-red half-n-half'
        options['speed'] = 60
        patt = PatternGradientScroll(self.led_state_internal, options=options)
        self.patterns.append(patt)

        options = {}
        options['cycles_active'] = 20
        options['cycles_inactive'] = 20
        options['phase_adjust'] = 0
        options['color'] = [0.0, 0.0, 1.0]
        patt = PatternFractalStrobeOff(self.led_state_internal, options=options)
        #patt.disableLEDs(mask)

        self.patterns.append(patt)


        options = {}
        options['cycles_active'] = 20
        options['cycles_inactive'] = 20
        options['phase_adjust'] = 20
        options['reverse_segments'] = True
        patt = PatternFractalStrobeOff(self.led_state_internal, options=options)
        #patt.disableLEDs(~ mask)

        self.patterns.append(patt)

        options = {}
        options['fraction_active'] = 0.7
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        options['min_effect'] = 0.0
        options['max_effect'] = 1.0
        options['min_period'] = 0.1
        options['max_period'] = 0.5
        options['time_start'] = common.runtime()+2.0
        options['time_fadein'] = 3.0
        #patt = PatternTwinkleColor(self.led_state_internal, options=options)
        #self.patterns.append(patt)


    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['end_after_lifetime'] = True
        options['time_lifetime'] = 20.0
        return options


class ProgramEmikoTchstTchst(LEDGardenProgram):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        index = np.linspace(0.0, common.kNumLEDs-1, common.kNumLEDs)

        mask = index > (common.kNumLEDs-1)/2.0
        mask = index%2 == 0

        #options = {}
        #options['gradient'] = 'blue-red half-n-half'
        #options['speed'] = 60
        #patt = PatternGradientScroll(self.led_state_internal, options=options)
        #self.patterns.append(patt)

        options = {}
        options['cycles_active'] = 20
        options['cycles_inactive'] = 20
        options['phase_adjust'] = 0
        options['color'] = [0.0, 1.0, 0.0]
        patt = PatternFractalStrobeOn(self.led_state_internal, options=options)
        self.patterns.append(patt)

        options = {}
        options['cycles_active'] = 20
        options['cycles_inactive'] = 20
        options['phase_adjust'] = 20
        options['reverse_segments'] = True
        options['color'] = [1.0, 0.5, 0.0]
        patt = PatternFractalStrobeOn(self.led_state_internal, options=options)
        self.patterns.append(patt)


        options = {}
        options['fraction_active'] = 0.7
        options['color'] = np.array([1.0, 1.0, 1.0], dtype=common.k_default_dtype)
        options['min_effect'] = 0.0
        options['max_effect'] = 1.0
        options['min_period'] = 0.1
        options['max_period'] = 0.5
        options['time_start'] = common.runtime()+5.0
        options['time_fadein'] = 5.0
        patt = PatternTwinkleColor(self.led_state_internal, options=options)
        self.patterns.append(patt)


    def defaultProgramOptions(self):
        options = super().defaultProgramOptions()
        options['end_after_lifetime'] = True
        options['time_lifetime'] = 20.0
        return options