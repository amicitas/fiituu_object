#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  Define some color handling utilites for the LED garden project.

"""

from collections import OrderedDict
import numpy as np

from . import common

gradients = OrderedDict()

# Rainbow.
grad = OrderedDict()
grad['red'] = [
    (0.0, 1.0)
    ,(0.2, 1.0)
    ,(0.3, 1.0)
    ,(0.5, 0.0)
    ,(0.7, 0.0)
    ,(1.0, 0.5)]
grad['green'] = [
    (0.0, 0.0)
    ,(0.2, 0.5)
    ,(0.3, 1.0)
    ,(0.5, 1.0)
    ,(0.7, 0.0)
    ,(1.0, 0.0)]
grad['blue'] = [
    (0.0, 0.0)
    ,(0.2, 0.0)
    ,(0.3, 0.0)
    ,(0.5, 0.0)
    ,(0.7, 1.0)
    ,(1.0, 1.0)]
gradients['rainbow'] = grad


# Rainbow Cycle.
grad = OrderedDict()
grad['red'] = [
    (0.00, 1.0)
    ,(0.17, 1.0)
    ,(0.25, 1.0)
    ,(0.42, 0.0)
    ,(0.58, 0.0)
    ,(0.83, 0.5)
    ,(1.00, 1.0)]
grad['green'] = [
    (0.00, 0.0)
    ,(0.17, 0.5)
    ,(0.25, 1.0)
    ,(0.42, 1.0)
    ,(0.58, 0.0)
    ,(0.83, 0.0)
    ,(1.00, 0.0)]
grad['blue'] = [
    (0.00, 0.0)
    ,(0.17, 0.0)
    ,(0.25, 0.0)
    ,(0.42, 0.0)
    ,(0.58, 1.0)
    ,(0.83, 1.0)
    ,(1.00, 0.0)]
gradients['rainbow cycle'] = grad


# Green orange
grad = OrderedDict()
grad['red'] = [
    (0.0, 1.0)
    ,(0.33, 0.0)
    ,(0.66, 1.0)
    ,(1.00, 1.0)]
grad['green'] = [
    (0.0, 0.5)
    ,(0.33, 1.0)
    ,(0.66, 1.0)
    ,(1.00, 0.5)]
grad['blue'] = [
    (0.0, 0.0)
    ,(0.33, 0.0)
    ,(0.66, 0.0)
    ,(1.00, 0.0)]
gradients['rastaman'] = grad



# Blue, pink, yellow, orange, pink, blue.
grad = OrderedDict()
grad['red'] = [
    (0.0, 0.0)
    ,(0.12, 1.0)
    ,(0.25, 1.0)
    ,(0.50, 1.0)
    ,(0.75, 1.0)
    ,(1.00, 0.0)]
grad['green'] = [
    (0.0, 0.0)
    ,(0.12, 0.0)
    ,(0.25, 1.0)
    ,(0.50, 0.5)
    ,(0.75, 0.0)
    ,(1.00, 0.0)]
grad['blue'] = [
    (0.0, 1.0)
    ,(0.12, 0.5)
    ,(0.25, 0.0)
    ,(0.50, 0.0)
    ,(0.75, 1.0)
    ,(1.00, 1.0)]
gradients['miami sunset'] = grad


# Orange, Yellow, Orange, Cyan, Pink, Blue
grad = OrderedDict()
grad['red'] = [
    (0.00, 0.5)
    ,(0.12, 0.0)
    ,(0.27, 1.0)
    ,(0.46, 0.0)
    ,(0.70, 0.5)
    ,(0.90, 0.0)
    ,(1.00, 0.5)]
grad['green'] = [
    (0.00, 0.5)
    ,(0.12, 1.0)
    ,(0.27, 0.5)
    ,(0.46, 1.0)
    ,(0.70, 0.0)
    ,(0.90, 0.0)
    ,(1.00, 0.5)]
grad['blue'] = [
    (0.00, 0.0)
    ,(0.12, 0.0)
    ,(0.27, 0.0)
    ,(0.46, 1.0)
    ,(0.70, 0.5)
    ,(0.90, 1.0)
    ,(1.00, 0.0)]
gradients['wavy davey'] = grad


# Half blue, half red.
grad = OrderedDict()
grad['red'] = [
    (0.0, 1.0)
    ,(0.5, 1.0)
    ,(0.5, 0.0)
    ,(1.0, 0.0)]
grad['green'] =[
    (0.0, 0.0)
    ,(0.5, 0.0)
    ,(0.5, 0.0)
    ,(1.00, 0.0)]
grad['blue'] = [
    (0.0, 0.0)
    ,(0.5, 0.0)
    ,(0.5, 1.0)
    ,(1.00, 1.0)]
gradients['blue-red half-n-half'] = grad

# Red, orange, yellow, red
grad = OrderedDict()
grad['red'] = [
    (0.0, 1.0)
    ,(0.33, 1.0)
    ,(0.66, 1.0)
    ,(1.0, 1.0)]
grad['green'] =[
    (0.0, 0.0)
    ,(0.33, 0.5)
    ,(0.66, 1.0)
    ,(1.0, 0.0)]
grad['blue'] = [
    (0.0, 0.0)
    ,(0.33, 0.0)
    ,(0.66, 0.0)
    ,(1.0, 0.0)]
gradients['fire'] = grad

# Blue, cyan, aqua, blue
grad = OrderedDict()
grad['red'] = [
    (0.0, 0.0)
    ,(0.33, 0.0)
    ,(0.66, 0.0)
    ,(1.0, 0.0)]
grad['green'] =[
    (0.0, 0.0)
    ,(0.33, 0.5)
    ,(0.66, 1.0)
    ,(1.0, 0.0)]
grad['blue'] = [
    (0.0, 1.0)
    ,(0.33, 1.0)
    ,(0.66, 1.0)
    ,(1.0, 1.0)]
gradients['aquarius'] = grad

# Green, lightgreen, cyanish, yellowish, darkgreen, green
grad = OrderedDict()
grad['red'] = [
    (0.0, 0.0)
    ,(0.2, 0.5)
    ,(0.4, 0.0)
    ,(0.6, 0.25)
    ,(0.8, 0.0)
    ,(1.0, 0.0)]
grad['green'] =[
    (0.0, 1.0)
    ,(0.2, 1.0)
    ,(0.4, 1.0)
    ,(0.6, 1.0)
    ,(0.8, 0.5)
    ,(1.0, 1.0)]
grad['blue'] = [
    (0.0, 0.0)
    ,(0.2, 0.5)
    ,(0.4, 0.25)
    ,(0.6, 0.0)
    ,(0.8, 0.0)
    ,(1.0, 0.0)]
gradients['seaweed'] = grad


# Yellow, orange, lightyellow, yellow
grad = OrderedDict()
grad['red'] = [
    (0.0, 1.0)
    ,(0.33, 1.0)
    ,(0.66, 1.0)
    ,(1.0, 1.0)]
grad['green'] =[
    (0.0, 1.0)
    ,(0.33, 0.5)
    ,(0.66, 1.0)
    ,(1.0, 1.0)]
grad['blue'] = [
    (0.0, 0.0)
    ,(0.33, 0.0)
    ,(0.66, 0.5)
    ,(1.0, 0.0)]
gradients['mellow yellow'] = grad


# Pink, Red, Purple, Pink
grad = OrderedDict()
grad['red'] = [
    (0.0, 1.0)
    ,(0.33, 1.0)
    ,(0.66, 0.0)
    ,(1.0, 1.0)]
grad['green'] =[
    (0.0, 0.0)
    ,(0.33, 0.0)
    ,(0.66, 0.0)
    ,(1.0, 0.0)]
grad['blue'] = [
    (0.0, 1.0)
    ,(0.33, 0.0)
    ,(0.66, 0.5)
    ,(1.0, 1.0)]
gradients['barbie'] = grad


# Convert all the gradient definitions into numpy arrays.
for grad in gradients.values():
    for key in grad:
        grad[key] = np.array(grad[key])


def gradient(value_in, name):
    """
    Return colors given a set of gradient values and a gradient name.
    """
    if np.isscalar(value_in):
        value = np.array([value_in])
    else:
        value = value_in

    color =  getGradientColor(value, gradients[name])

    if np.isscalar(value_in):
        return color[0]
    else:
        return color


def getGradientColor(value, gradient):
    color = np.zeros((len(value), 3), dtype=common.k_default_dtype)
    for ii, color_name in enumerate(gradient):
        color[:, ii] = getGradentColorSingle(value, gradient[color_name])

    return color


def getGradentColorSingle(value, gradient):

    index = np.zeros(len(value), dtype=int)

    # First figure out what section we are in.
    found = np.zeros(len(value), dtype=bool)
    for ii in range(1, len(gradient)):
        mask = (value <= gradient[ii, 0]) & (~found)
        index[mask] = ii-1
        found |= mask
        #print(ii, mask, found)

    fraction = (value - gradient[index, 0])/(gradient[index+1, 0] - gradient[index, 0])

    #print('index: ', index, 'fraction: ', fraction)
    #print('gradient[index, 1]: ', gradient[index, 1], 'gradient[index+1, 1]', gradient[index+1, 1])
    color = (gradient[index+1, 1] - gradient[index, 1])*fraction + gradient[index, 1]

    return color