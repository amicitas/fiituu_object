#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
For testing rapid updates of ScatterPlotItem under various conditions.

(Scatter plots are still rather slow to draw; expect about 20fps)
"""



from time import sleep

from pyqtgraph.Qt import QtGui, QtCore, USE_PYSIDE
import numpy as np
import pyqtgraph as pg
from pyqtgraph.ptime import time

from ledgarden.ledgarden import common, LEDGarden

from mirutil import logging

class LEDGardenSimulator(QtCore.QObject):

    def __init__(self, parent=None, garden=None):
        super().__init__(parent=parent)

        global curve1, curve2, data, ptr, p, lastTime, fps, brushes


        self.log = logging.getLogger(self.__class__.__name__)

        # Start the LEDGarden
        if garden is None:
            garden = LEDGarden()
        self.garden = garden

        # Wait to allow the serial connection to get established.
        sleep(0.5)

        p = pg.plot()
        w = p.window()
        w.setGeometry(0, 0, 200, 1000)

        p.setRange(xRange=[-10, 10], yRange=[0, 150])
        p.hideAxis('bottom')
        p.hideAxis('left')
        p.disableAutoRange()

        #y = np.linspace(0, num_pixels-1, num_pixels)
        #y[num_pixels/2:] = y[:num_pixels/2]
        #x = np.zeros(num_pixels)
        #x[:num_pixels/2] = -0.5
        #x[num_pixels/2:] = 0.5

        y1 = np.linspace(0, common.kNumLEDs-1, common.kNumLEDs)
        x1 = np.zeros(common.kNumLEDs)+0.5

        y2 = np.linspace(0, common.kNumLEDs-1, common.kNumLEDs)
        x2 = np.zeros(common.kNumLEDs)-0.5

        # Instantiate the brushes in advance since this is slow.
        brushes = [pg.mkBrush([0,0,0]) for ii in range(common.kNumLEDs)]

        size = 5
        curve1 = pg.ScatterPlotItem(x=x1
                                   ,y=y1
                                   ,brush=brushes
                                   ,size=size,
                                   pxMode=True)
        curve1.setPen(pg.mkPen(None))

        curve2 = pg.ScatterPlotItem(x=x2
                                   ,y=y2
                                   ,brush=brushes
                                   ,size=size,
                                   pxMode=True)
        curve2.setPen(pg.mkPen(None))

        ptr = 0
        lastTime = time()
        fps = None


    def loop(self):
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.run)
        self.timer.start(0)

    def updateLoop(self):
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(0)

    def run(self):
        self.garden.run()
        self.update()

    def update(self):
        global curve1, curve2, data, ptr, p, lastTime, fps, brushes

        time_start = time()

        #self.garden.run()

        time_run_finished = time()

        p.clear()

        for ii in range(common.kNumLEDs):
            color = QtGui.QColor(
                self.garden.led_state['color'][ii,0]*255,
                self.garden.led_state['color'][ii,1]*255,
                self.garden.led_state['color'][ii,2]*255)
            brushes[ii].setColor(color)

        time_brush_finished = time()

        curve1.setBrush(brushes)
        curve2.setBrush(brushes)

        p.addItem(curve1)
        p.addItem(curve2)

        time_finish = time()

        dt = time_finish - lastTime
        lastTime = time_finish
        if fps is None:
            fps = 1.0/dt
        else:
            s = np.clip(dt*3., 0, 1)
            fps = fps * (1-s) + (1.0/dt) * s
        p.setTitle('%0.2f fps' % fps)
        p.repaint()
        #app.processEvents()  ## force complete redraw for every plot

        dtime_cycle = time_finish - time_start
        dtime_calc = time_run_finished - time_start
        dtime_transfer = 0.0
        dtime_brush = time_brush_finished - time_run_finished
        dtime_render = time_finish - time_run_finished

        # self.log.debug('Python cycle time:  {} ms, Program time: {} ms, Brush time: {} ms, Render time: {} ms'.format(
        #     int(dtime_cycle*1000.), int(dtime_calc*1000.), int(dtime_brush*1000.), int(dtime_render*1000.)))

## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])

    simulator = LEDGardenSimulator(app)
    simulator.loop()

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        app.exec_()


