#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  Start the LEDGarden with a terminal interface.

Notes:
  This application will only run on Linux or OS X. Currently the code
  for capturing key strokes is posix specific.

Programming Notes:
  The way I am handling the simulator is maybe a little ugly, but this
  does appear to be working reasonably well.
"""
# ------------------------------------------------------------------------------

from PyQt4 import QtGui, QtCore
import signal

import sys, select
import termios

from mirutil import logging
import time

from ledgarden.ledgarden import common, LEDGarden
from . import serial_arduino


def getLEDGardenSimulatorObject(*args, **kwargs):
    """
    Return an LEDGarden Simulator object.

    This is in a separate funciton so that we don't import
    the simulator module unless necessary.  I don't want a
    general dependency on the pyqtgraph library.
    """
    print('Enabling simulator.')
    from ledgarden.simulator import LEDGardenSimulator
    return LEDGardenSimulator(*args, **kwargs)


class LEDGardenTerminal(QtCore.QObject):
    """
    A terminal (text) interface to the LEDGarden.

    This is specifically diffirent from a GUI version as it captures
    keyboard input directly from STDIN instead of from the gui.
    Actually, I seem to be able to use this fine in the simulator
    even though it uses QtGui.  So there is a way to make this work.
    """

    signalStatus = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)

        self.log = logging.getLogger(self.__class__.__name__)

        if common.options['arduino_usb_connection']:
            serial_arduino.connectToArduino()

        # Setup the key object and the key_thread.
        self.key = KeyboardReader()
        self.key_thread = QtCore.QThread()
        self.key.moveToThread(self.key_thread)
        self.key_thread.started.connect(self.key.startLoop)
        self.key_thread.finished.connect(self.key.cleanup)
        self.key_thread.terminated.connect(self.key.testTerminated)
        self.key.signalQuitRequest.connect(self.stop)

        self.key_thread.start()

        # Setup the worker object and the worker_thread.
        self.worker = WorkerObject()
        self.worker_thread = QtCore.QThread()
        self.worker.moveToThread(self.worker_thread)
        self.worker_thread.started.connect(self.worker.startLoop)
        self.worker_thread.start()

        if common.options['enable_simulator']:
            self.simulator = getLEDGardenSimulatorObject(parent, garden=self.worker.garden)
            self.simulator.updateLoop()

        # Make any cross object connections.
        self._connectSignals()


    def run(self):
        self.parent().processEvents()
        self.garden.run()


    def startLoop(self):
        self.garden = LEDGarden()
        while True:
            self.garden.run()


    def _connectSignals(self):
        self.key.signalEventButton.connect(self.worker.garden.actionButtonPress)
        self.key.signalEventButtonToggle.connect(self.worker.garden.actionButtonToggle)
        self.key.signalEventButtonMode.connect(self.worker.garden.cycleToNextMode)
        self.key.signalEventButtonBrightessUp.connect(self.worker.garden.increaseBrightness)
        self.key.signalEventButtonBrightessDown.connect(self.worker.garden.decreaseBrightness)
        self.key.signalEventButtonSpeedUp.connect(self.worker.garden.increaseSpeed)
        self.key.signalEventButtonSpeedDown.connect(self.worker.garden.decreaseSpeed)
        self.key.signalEventRequestDebug.connect(self.worker.garden.printDebugInfo)
        self.key.signalEventRequestCrazy.connect(self.worker.garden.actionCrazyMode)
        self.key.signalEventRequestIncreaseColorShift.connect(self.worker.garden.increaseColorShift)
        self.key.signalEventRequestDecreaseColorShift.connect(self.worker.garden.decreaseColorShift)
        self.key.signalEventRequestIncreaseColorSwap.connect(self.worker.garden.increaseColorSwap)
        self.key.signalEventRequestDecreaseColorSwap.connect(self.worker.garden.decreaseColorSwap)

    def stop(self):
        self.log.debug('Stopping all threads and closing the application.')

        if common.options['enable_simulator']:
            self.simulator.deleteLater()

        self.key_thread.quit()
        self.worker_thread.quit()

        self.key_thread.wait()
        self.worker_thread.wait()

        # Now quit the main application.
        self.parent().quit()


class WorkerObject(QtCore.QObject):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.log = logging.getLogger(self.__class__.__name__)

        self.garden = LEDGarden()


    def run(self):
        #self.log.debug('Thread: {}'.format(int(QtCore.QThread.currentThreadId())))
        self.garden.run()

    @QtCore.pyqtSlot()
    def startLoop(self):
        self.log.debug('Starting: worker loop.')
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.run, type=QtCore.Qt.DirectConnection)
        self.timer.start(10)
        self.log.debug('Loop started.')


class KeyboardReader(QtCore.QObject):

    signalQuitRequest =  QtCore.pyqtSignal()
    signalRequestPrintStatus = QtCore.pyqtSignal()

    signalEventButton = QtCore.pyqtSignal(int)
    signalEventButtonToggle = QtCore.pyqtSignal(int)
    signalEventButtonMode = QtCore.pyqtSignal()
    signalEventButtonBrightessUp = QtCore.pyqtSignal()
    signalEventButtonBrightessDown = QtCore.pyqtSignal()
    signalEventButtonSpeedUp = QtCore.pyqtSignal()
    signalEventButtonSpeedDown = QtCore.pyqtSignal()
    signalEventRequestDebug = QtCore.pyqtSignal()
    signalEventRequestCrazy = QtCore.pyqtSignal()
    signalEventRequestIncreaseColorShift = QtCore.pyqtSignal()
    signalEventRequestDecreaseColorShift = QtCore.pyqtSignal()
    signalEventRequestIncreaseColorSwap = QtCore.pyqtSignal()
    signalEventRequestDecreaseColorSwap = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.log = logging.getLogger(self.__class__.__name__)

        # Save the terminal settings
        self.fd = sys.stdin.fileno()
        self.new_term = termios.tcgetattr(self.fd)
        self.old_term = termios.tcgetattr(self.fd)

        # New terminal setting unbuffered
        self.new_term[3] = (self.new_term[3] & ~termios.ICANON & ~termios.ECHO)
        termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.new_term)

    @QtCore.pyqtSlot()
    def startLoop(self):
        """
        Start a loop to check for user input.

        Use a timer here to allow the Qt event loop to run
        between succesive calls to handleKeyboardInput.
        """
        self.log.debug('Starting: key handling loop.')
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.handleKeyboardInput)
        self.timer.start(0)


    def handleKeyboardInput(self):
        # If there's input ready, then read one character at a time.
        while sys.stdin in select.select([sys.stdin], [], [], 1.0)[0]:
            char = sys.stdin.buffer.read(1)

            # I don't know how to read everytihng off the input buffer.
            # So for now just check to see if we got an escape char
            # then try to read the expected bytes for arrow keys.
            if char == b'\x1b':
                char += sys.stdin.buffer.read(2)

            #self.log.info('User input: {}'.format(char))
            self.parseKeyboardInput(char)


    def parseKeyboardInput(self, char):

        self.log.debug('Parsing keybord input: '+char.decode('ascii'))

        if char == b'\x1b[A':
            #print("Up arrow")
            self.signalEventButtonBrightessUp.emit()
        elif char == b'\x1b[B':
            #print("Down arrow")
            self.signalEventButtonBrightessDown.emit()
        elif char == b'\x1b[C':
            #print("Right arrow")
            self.signalEventButtonSpeedUp.emit()
        elif char == b'\x1b[D':
            #print("Left arrow")
            self.signalEventButtonSpeedDown.emit()
        else:
            char = char.lower()
            if char == b'q':
                self.log.debug('Sending quit request.')
                self.signalQuitRequest.emit()
            elif char == b'm':
                self.signalEventButtonMode.emit()
            elif char == b'1':
                self.signalEventButton.emit(0)
            elif char == b'2':
                self.signalEventButton.emit(1)
            elif char == b'3':
                self.signalEventButton.emit(2)
            elif char == b'4':
                self.signalEventButton.emit(3)
            elif char == b'5':
                self.signalEventButton.emit(4)
            elif char == b'6':
                self.signalEventButtonToggle.emit(0)
            elif char == b'7':
                self.signalEventButtonToggle.emit(1)
            elif char == b'8':
                self.signalEventButtonToggle.emit(2)
            elif char == b'9':
                self.signalEventButtonToggle.emit(3)
            elif char == b'0':
                self.signalEventButtonToggle.emit(4)
            elif char == b'c':
                self.signalEventRequestCrazy.emit()
            elif char == b'd':
                self.signalEventRequestDebug.emit()
            elif char == b"'":
                self.signalEventRequestDecreaseColorShift.emit()
            elif char == b',':
                self.signalEventRequestIncreaseColorShift.emit()
            elif char == b'a':
                self.signalEventRequestDecreaseColorSwap.emit()
            elif char == b'o':
                self.signalEventRequestIncreaseColorSwap.emit()
            else:
                self.signalRequestPrintStatus.emit()


    def cleanup(self):
        print('keyboardReader.cleanup')
        termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.old_term)


    def testTerminated(self):
        print('keyboardReader.Terminated')


    def testQuitSignal(self):
        print('keyboardReader.testQuitSignal')


def parseCommandLineOptions(options_commandline):

    common.options['arduino_usb_connection'] = not options_commandline.offline
    common.options['enable_simulator'] = options_commandline.simulator

    print(options_commandline.offline, common.options['arduino_usb_connection'])

def getApp(options_commandline):

    parseCommandLineOptions(options_commandline)

    print('Starting Qt application')

    if common.options['enable_simulator']:
        QtGui.QApplication.setGraphicsSystem('raster')
        app = QtGui.QApplication([])
    else:
        # For some reasion when I use the QtGui application my program hangs after
        # a while. I don't understand this.
        app = QtCore.QCoreApplication([])

    print('Starting the LEDGarden.')
    daemon = LEDGardenTerminal(app)
    app.aboutToQuit.connect(daemon.stop)

    # Setup handling for a keyboard interupt signal.
    signal.signal(signal.SIGINT, lambda *a: daemon.stop())

    # Setup a timer to check for keyboard interrupts.
    # This just forces Qt to run the python interpreter occasionally.
    timer = QtCore.QTimer()
    timer.timeout.connect(lambda: None)
    timer.start(500)

    return app
