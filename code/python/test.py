#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  Main script to run the LED Garden.

"""

from ledgarden.ledgarden import *

if __name__ == "__main__":

    garden = LEDGarden()
    # Wait to allow the serial connection to get established.
    time.sleep(0.5)
    garden.loop()