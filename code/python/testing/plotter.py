
import time

import LEDGarden
import LEDGardenCommon as common

from mirutil.plot import mirplot

class LEDGardenPlotter(LEDGarden.LEDGarden):

    def __init__(self, user_config=None):
        super().__init__(user_config)

        self.led_history = [[] for ii in range(self.config['num_leds'])]

    def getDefaultConfig(self):
        config = super().getDefaultConfig()
        config['arduino_usb_connection'] = False

        return config

    def run(self):
        super().run()

        for ii, state in enumerate(self.led_state):
            self.led_history[ii].append(state['value'])

        # Slow the loop down, otherwise time.time() does not have enough resolution
        # to properly calculate the patterns.
        time.sleep(20/1000.)

    def loop(self):
        while common.timeElapsed() < 20.0:
            self.run()

if __name__ == "__main__":
    
    garden = LEDGardenPlotter()
    time.sleep(0.5)
    garden.loop()

    p = mirplot.quickplot([{'name':'individual'
                            ,'y':garden.led_history[0]
                            ,'color':'purple'}
                           ,{'name':'individual'
                             ,'y':garden.led_history[16]
                            ,'color':'red'}
                           ,{'name':'individual'
                             ,'y':garden.led_history[32]
                            ,'color':'black'}])

