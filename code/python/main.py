#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
"""
Authors:
  | Novimir Antoniuk Pablant <npablant@pppl.gov>

Purpose:
  Main script to run the LED Garden.

"""

import sys
from PyQt4 import QtCore

import argparse

from ledgarden import ledgarden_terminal

if __name__=='__main__':

    parser = argparse.ArgumentParser(description="The LED garden project.")
    parser.add_argument('--offline'
                        ,action='store_true'
                        ,help='Run in offline mode with no Arduino connected.')
    parser.add_argument('--simulator'
                        ,action='store_true'
                        ,help='Run the simulator.')
    args = parser.parse_args()

    app = ledgarden_terminal.getApp(args)
    sys.exit(app.exec_())