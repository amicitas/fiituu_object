
#include "fiituu.h"
#include "fiituu_config.h"

/* ========================================================================== */
/* ========================================================================== */
/* FiiTuu */
/* ========================================================================== */
/* ========================================================================== */



/* -----------------------------------------------------------------------------
   This is the main program that will be called from start().
   --------------------------------------------------------------------------- */
void FiiTuu::init(void) {

  if (logLevel(NONE)) {
      Serial.println("INITIALIZING THE FIIRE TUUBE.");
  }

  // Initialize serial communication at 115200 bps
  //Serial.begin(115200);
  // Initialize serial communication at 250000 bps
  //Serial.begin(250000);
  // Initialize serial comminication at 500000 bps
  // This seems to work ok.
  //Serial.begin(500000);
  // Initialize serial comminication at 1000000 bps
  // At the moment this seems to work.
  Serial.begin(1000000);

  pinMode(kPinLED, OUTPUT);

  // Initialize the NeoPixel object.
  // So this is apparently not correct C++.  I guess I need to instantiate
  // the inner class during instantiation of my outer class.
  //
  //strip = Adafruit_NeoPixel(300, kPinNeoPixel, NEO_GRB + NEO_KHZ800);

  // Intitialze the LED state.
  //Serial.println("FiiTuuConfig");
  //FiiTuuConfig(led_state_array);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off' 

  Serial.print("numPixels: "); 
  Serial.println(strip.numPixels());
  strip.setPixelColor(0, 55, 55, 55);
  Serial.print("Test setting pixel: ");
  Serial.println(strip.getPixelColor(0));


  // Initialize the Buttons.
  initButtons();

  runtime = 0;

  // Just some debugging.
  if (logLevel(DEBUG_INIT)) {
    Serial.println("=== LED STATE AFTER INITIALIZATION ===");
    for (int ii = 0; ii < kNumLeds; ii++){
      Serial.print(ii);
      Serial.print(": ");
      Serial.println(strip.getPixelColor(ii));
    }
  }
  
}


/* -----------------------------------------------------------------------------
   Initialize the buttons.
   --------------------------------------------------------------------------- */
void FiiTuu::initButtons(void) {

  // Load the button configuration.
  buttonConfig(button_state_array);

  // Setup analog pins for button input.
  // I am likely being repetative here, but thats OK.
  for (int ii = 0; ii < kNumButtons; ii++){  
    pinMode(button_state_array[ii].analog_pin, INPUT_PULLUP);
  }

  // Initialize the current and previous state.
  clearButtonState();

}

/* -----------------------------------------------------------------------------
   This is the main routine that called from inside loop() in the main sketch.
   --------------------------------------------------------------------------- */
void FiiTuu::run(void) {
  
  if (loglevel >= DEBUG_CYCLE) {
    Serial.println("=== NEW CYCLE ===");
    }
  
  clearIo();
  raspberrySendReady();

  // Update the buttons and send the state to python.
  updateButtons();
  sendButtonState();

  // Update the LED settings.
  digitalWrite(kPinLED, HIGH);
  recieveLEDArray();
  digitalWrite(kPinLED, LOW);

  strip.show();

  if (loglevel >= DEBUG_CYCLE) {

    for (int ii = 0; ii < kNumLeds; ii++){
      Serial.print(ii);
      Serial.print(":  ");
      Serial.println(strip.getPixelColor(ii));
    }

    Serial.print("Cycle time:  ");
    Serial.println(millis() - runtime);
    Serial.println("=== END CYCLE ===");
  }


  runtime = millis();


}


/* -----------------------------------------------------------------------------
   Set the current and previoust button setting to false for every Button.
   --------------------------------------------------------------------------- */
void FiiTuu::clearButtonState(void) {
  for (int ii = 0; ii < kNumButtons; ii++){
    button_state_array[ii].last_state = false;
    button_state_array[ii].current_state = false;
  }
}


/* -----------------------------------------------------------------------------
   Check to see if any of the buttons have been pressed.  If so, take the
   appropriate action.
   --------------------------------------------------------------------------- */
void FiiTuu::updateButtons(void) {
  for (int ii = 0; ii < kNumButtons; ii++){
    updateButtonState(ii);
    if ((button_state_array[ii].last_state == false) &&
        (button_state_array[ii].current_state == true)){
      // A button press was detected, take the appropriate action.
      if (logLevel(DEBUG_ALL)) {
          Serial.print("Button ");
          Serial.print(ii);
          Serial.println(" was pressed");
      }
    }
  }
}

/* -----------------------------------------------------------------------------
   Update the internal state of the current button.

   Note: As written this will only handle simulteanious button presses if
         each button goes to a different analog pin. Otherwise this might
         give strange results.
   --------------------------------------------------------------------------- */
void FiiTuu::updateButtonState(int ii){

  int pin_value = 0;

  // Save the previous state.
  button_state_array[ii].last_state = button_state_array[ii].current_state;

  // Here I check the arduino for the current button state.
  pin_value = analogRead(button_state_array[ii].analog_pin);

  button_state_array[ii].current_state = (pin_value>button_state_array[ii].analog_range[0] 
                                          && pin_value<button_state_array[ii].analog_range[1]);
  
  if (logLevel(DEBUG_ALL)) {
      Serial.print("Button ");
      Serial.print(ii);
      Serial.print(" state: ");
      Serial.println(button_state_array[ii].current_state);
      Serial.print(" pin value: ");
      Serial.println(pin_value);
  }

}


void FiiTuu::recieveLEDArray() {
    String str;
    bool waiting = true;
  
    unsigned char value[1];
    int status; 
  
    raspberrySendNext();
    while (waiting) {
      if  (Serial.available() > 0) {
        str = Serial.readStringUntil('\n');
        if (logLevel(DEBUG_ALL)) {
            Serial.print("ARDUINO RECIEVE: ");
            Serial.println(str);
        }
        if (str=="LED"){
          readLEDArray();
          raspberrySendNext();
          //waiting=false;
        }
        else if (str=="DONE"){
          waiting = false;
        }
        else if (str=="OPTION_ENABLE_MIRROR"){
          status = Serial.readBytes(value, 1);
          optionEnableMirror = value[0];
        }
        else if (str=="OPTION_MIRROR_REVERSED"){
          status = Serial.readBytes(value, 1);
          optionMirrorReversed = value[0];
        } 
      }
    }
}

/* -----------------------------------------------------------------------------
   Read anything that is currently on the read buffer.
   This is used when there is nothing in particular that is expected.
   --------------------------------------------------------------------------- */
void FiiTuu::clearIo() {
    char str;
    Serial.flush();
    while (Serial.available() > 0) {
        str = Serial.read();
        Serial.print(str);
    }
    Serial.print('\n');
}

/* -----------------------------------------------------------------------------
   Read anything that is currently on the read buffer.
   This is used when there is nothing in particular that is expected.
   --------------------------------------------------------------------------- */
void FiiTuu::checkio() {
    String str;
    Serial.flush();
    while (Serial.available() > 0) {
        str = Serial.readStringUntil('\n');
        Serial.print("ARDUINO RECIEVE: ");
        Serial.println(str);
    }
}

/* -----------------------------------------------------------------------------
   Receive a portion of the LED array.
   --------------------------------------------------------------------------- */
void FiiTuu::readLEDArray() {
  int starting_led = 0;
  int num_leds = 0;
  Rgb24Bit rgb;

  int temp;

  starting_led = serialReadUnsignedShort();
  //Serial.print("ARDUINO RECIEVE: ");
  //Serial.println(starting_led);

  num_leds = serialReadUnsignedShort();
  //Serial.print("ARDUINO RECIEVE: ");
  //Serial.println(num_leds);
  
  for (uint16_t ii=starting_led; ii<starting_led+num_leds; ii++) {
    
    rgb = serialReadRgb24Bit();
    strip.setPixelColor(ii, rgb.rgb[0], rgb.rgb[1], rgb.rgb[2]);
    
    // If this option is set then mirror the recieved values to the
    // second half of the strip. 
    if (optionEnableMirror) {
      if (optionMirrorReversed) {
        strip.setPixelColor(kNumLeds-1-ii, rgb.rgb[0], rgb.rgb[1], rgb.rgb[2]);
      } else {
        strip.setPixelColor(ii+kNumLeds/2, rgb.rgb[0], rgb.rgb[1], rgb.rgb[2]);
      } 
    }
    
    // Debugging to let me see each pixel I receive.
    /*
    Serial.print("ARDUINO RECIEVE (");
    Serial.print(ii);
    Serial.print("): ");
    Serial.print(int(rgb.rgb[0]));
    Serial.print(", ");
    Serial.print(int(rgb.rgb[1]));
    Serial.print(", "); 
    Serial.print(int(rgb.rgb[2]));
    Serial.print(" | color: ");
    Serial.print(strip.Color(rgb.rgb[0], rgb.rgb[1], rgb.rgb[2]));
    Serial.print(" | strip: ");    
    Serial.println(strip.getPixelColor(ii));
    */
  }

  // This ensures that the read buffer is clear.
  // I had this here for debugging purposes, but
  // it looks like everthing is working at the moment.
  /*
  Serial.println("Checking the read buffer.");
  while (Serial.peek() != -1) {
    temp = Serial.read();
    Serial.print("Next byte on the stack: ");
    Serial.println(temp);
  }
  Serial.println("No data available on buffer.");
  */
}


int FiiTuu::serialReadUnsignedShort() {
  unsigned char byte_array[2];
  int int_value;
  int status;

  status = Serial.readBytes(byte_array, 2);
  int_value = byte_array[0]*256 + byte_array[1];

  return int_value;
}


Rgb24Bit FiiTuu::serialReadRgb24Bit() {
  Rgb24Bit rgb_struct;
  int status;

  status = Serial.readBytes(rgb_struct.rgb, 3);

  return rgb_struct;
}


void FiiTuu::raspberrySendReady() {
  Serial.print("OK\n");
}


void FiiTuu::raspberrySendNext() {
  Serial.print("NEXT\n");
}


void FiiTuu::sendButtonState() {
    Serial.print("BUTTON\n");
    Serial.write(kNumButtons);
    for (int ii=0; ii<kNumButtons; ii++) {
        Serial.write(button_state_array[ii].current_state);
    }
    Serial.flush();
}

