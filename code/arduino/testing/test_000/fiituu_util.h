

/* ==========================================================================
   ==========================================================================
   LED GARDEN UTILITIES

   Define various utilites needed for the LED Garden Object.
   ==========================================================================
   ========================================================================== */


#ifndef fiituu_util_h
#define fiituu_tuil_h

#include "fiituu_config.h"

bool logLevel(logging_levels level);

#endif
