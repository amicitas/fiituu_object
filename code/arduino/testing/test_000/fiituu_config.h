

#ifndef fiituu_config_h
#define fiituu_config_h


#include <Adafruit_NeoPixel.h>
#include "Arduino.h"

//const double kPi = atan(1.0)*4;

// Setup options
extern bool optionEnableMirror;
extern bool optionMirrorReversed;

// Setup Arduino paramaters
const int kPinLED = 13;
const int kPinNeoPixel = 23;

// Setup LED parameters
const int kNumLeds = 300;
const int kStartingLed = 0;


// Define button parameters.
// Assume I have 5 buttons. Four control indivdual programs, and one 
// controls the overall mode.
const int kNumButtons = 5;
struct ButtonState {
  bool current_state;
  bool last_state;
  int analog_range[2];
  int analog_pin;
};


// Setup some global configuration variables.
enum logging_levels {NONE, DEBUG, DEBUG_INIT, DEBUG_CYCLE, DEBUG_ALL};
extern logging_levels loglevel;


// Define function definitions.
void buttonConfig(ButtonState x[]);

extern Adafruit_NeoPixel strip;
  
#endif

