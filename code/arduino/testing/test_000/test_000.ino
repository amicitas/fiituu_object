
#include "fiituu.h"

FiiTuu fiituu;

void setup() {

  Serial.println("INITIALIZING THE HEARTPATH OBJECT.");

  /* Call Garden.init() to setup the TLCs.*/
  fiituu.init();
  
  delay(100);
}

void loop() {
  
  // Eventually everthing will happen in here.
  fiituu.run();
}
