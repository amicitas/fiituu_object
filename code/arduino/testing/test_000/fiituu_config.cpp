
#include "arduino.h"
#include "fiituu_config.h"

bool optionEnableMirror = true;
bool optionMirrorReversed = false;

//logging_levels loglevel = DEBUG_ALL;
//logging_levels loglevel = DEBUG;
logging_levels loglevel = DEBUG_INIT;
//logging_levels loglevel = NONE;

void buttonConfig(ButtonState x[]) {
    // Set the appropriate analog ranges for the available buttons.
    // This will be unique for my particular hardware configuration.

    x[0].analog_pin = A1;
    x[0].analog_range[0] = 0;
    x[0].analog_range[1] = 900;

    x[1].analog_pin = A0;
    x[1].analog_range[0] = 0;
    x[1].analog_range[1] = 900;

    x[2].analog_pin = A3;
    x[2].analog_range[0] = 0;
    x[2].analog_range[1] = 900;

    x[3].analog_pin = A2;
    x[3].analog_range[0] = 0;
    x[3].analog_range[1] = 900;

    x[4].analog_pin = A4;
    x[4].analog_range[0] = 0;
    x[4].analog_range[1] = 900;
}

Adafruit_NeoPixel strip = Adafruit_NeoPixel(300, kPinNeoPixel, NEO_GRB + NEO_KHZ800);

