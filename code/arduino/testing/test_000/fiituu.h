
/* ==========================================================================
   ==========================================================================
   LED GARDEN

   Code for my LED garden art project for Burning Man 2012.



   The idea here is that I am going to have a 4 button pannel.  Pushing the
   buttons will either lauch new patterns, or change the existing programs.

   Mode 1. Twinkle mode

     When inactive run a basic twinkle pattern.  Evertime a button is pressed,
     first fade out the twinkle patten, then start the new pattern:
      - Hotaru:           Randomly lauches a firefly program in one or more 
                          colors.
      - Heartbeat:        Launches a heartbeat/twinkle program.
      - Frantic Twinkle:  Twinkling, but more frantic. 
      - Fire:             Similar to flicking flames.

   Mode 2: Dark mode.

     In this mode the garden is completely dark.  Pushing the buttons launches
     different programs.
      - Hotaru:    Randomly lauches a firefly program in one or more colors.
      - Heartbeat: Launches a heartbeat/twinkle program.
      - Twinkle:   Lauches a twinkle pattern.

   Mode 3. Heatbeat mode

     Here I start with my favorite Heatbeat mode.  Pushing a button may or may
     not fade out the heatbeat pattern, then as in all others, start up a new
     pattern.
      - Heartbeat Color: Changes the color of the heartbeat.
      - Hotaru:          Fades out the heat beat, and moves to a firefly 
                         pattern.
      - Frantic Twinkle: 



   To start I am going to the simplest thing and simply create the four
   programs ahead of time.  Pressing the button will relaunch the given
   program, rather than creating a new one.

   ==========================================================================
   ========================================================================== */


#ifndef fiituu_h
#define fiituu_h

#include <Adafruit_NeoPixel.h>
#include "fiituu_config.h"
#include "fiituu_util.h"

struct Rgb24Bit {
  unsigned char rgb[3];
};

/* ========================================================================== */
/* The main LEDGarden class for the entire library. */
class FiiTuu {

 public:
  // Declare Variables
  ButtonState button_state_array[kNumButtons];
  //Adafruit_NeoPixel strip;
  
  //Declare Variables
  long unsigned runtime;
  long unsigned cycletime;

  // Declare Functions
  void init(void);
  void initButtons(void);
  void run(void);
  void clearLEDSetting(void);
  void cleanup(void);
  void updateTLCs(void);

  void clearButtonState(void);
  void updateButtons(void);
  void updateButtonState(int ii);
  void buttonPressAction(int ii);



  // Fuctions that deal with serial io.
  void recieveLEDArray();
  void checkio();
  void clearIo();
  void readLEDArray();
  int serialReadUnsignedShort();
  Rgb24Bit serialReadRgb24Bit();
  void raspberrySendReady();
  void raspberrySendNext();
  void sendButtonState();

};


#endif

