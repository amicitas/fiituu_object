# README #

The Fiituu Object is a art piece for PortalBurn 2016.  This repository contains the code required to control the LED lighting for the project.

This is currently setup to control a 300 LED strip of WS2812B RGB 'NeoPixels'.  Currently I am controling an Arduino Mega ADK from my MacBook Pro.  This works reasonably well on my MacBook, giving me something like 30 frames per second, which seems to be limited by the Arduino speed.  I tried this out on a Raspberry Pi 1B, and the python computations were way too slow.

### Installation ###

This currently runs using Python 3 and the Arduino IDE.

1. Install the Arduino IDE.  Open test_000.ino.  Verify and upload to the Arduio Mega ADK.

2. Start the python script using:

```
#!python

python main.py
```

3. Have fun!